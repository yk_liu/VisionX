/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::PointCloud
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CaptureNumberOfFrames.h"
#include "PointCloudStatechartContext.generated.h"
namespace armarx::PointCloud
{
    // DO NOT EDIT NEXT LINE
    CaptureNumberOfFrames::SubClassRegistry CaptureNumberOfFrames::Registry(CaptureNumberOfFrames::GetName(), &CaptureNumberOfFrames::CreateInstance);



    void CaptureNumberOfFrames::onEnter()
    {
        std::string providerName = in.getProviderName();
        int numberOfFrames = in.getNumberOfFrames();
        PointCloudStatechartContext* context = getContext<PointCloudStatechartContext>();

        visionx::CapturingPointCloudProviderInterfacePrx providerPrx;

        try
        {
            providerPrx = context->getIceManager()->getProxy<visionx::CapturingPointCloudProviderInterfacePrx>(providerName);
        }
        catch (...)
        {

        }

        if (providerPrx)
        {
            providerPrx->startCaptureForNumFrames(numberOfFrames);
            emitSuccess();
        }
        else
        {
            emitFailure();
        }
    }


    void CaptureNumberOfFrames::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr CaptureNumberOfFrames::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new CaptureNumberOfFrames(stateData));
    }
}
