/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     Peter Kaiser (peter dot kaiser at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "AffordancePipelineGuiConfigDialog.h"
#include "ui_AffordancePipelineGuiConfigDialog.h"

#include <QTimer>
#include <QPushButton>
#include <QMessageBox>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

#include <VisionX/interface/core/PointCloudProviderInterface.h>
#include <VisionX/interface/core/PointCloudProcessorInterface.h>
#include <VisionX/interface/components/PointCloudSegmenter.h>
#include <VisionX/interface/components/PrimitiveMapper.h>
#include <VisionX/interface/components/AffordanceExtraction.h>
#include <VisionX/interface/components/AffordancePipelineVisualization.h>

#include <RobotAPI/interface/core/RobotState.h>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include <IceUtil/UUID.h>

using namespace armarx;

AffordancePipelineGuiConfigDialog::AffordancePipelineGuiConfigDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::AffordancePipelineGuiConfigDialog),
    uuid(IceUtil::generateUUID())
{
    ui->setupUi(this);

    connect(this->ui->buttonBox, SIGNAL(accepted()), this, SLOT(verifyConfig()));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);

    pointCloudSegmenterProxyFinder = new IceProxyFinder<visionx::PointCloudSegmenterInterfacePrx>(this);
    primitiveExtractorProxyFinder = new IceProxyFinder<visionx::PrimitiveMapperInterfacePrx>(this);
    affordanceExtractionProxyFinder = new IceProxyFinder<AffordanceExtractionInterfacePrx>(this);
    pipelineVisualizationProxyFinder = new IceProxyFinder<AffordancePipelineVisualizationInterfacePrx>(this);
    robotStateComponentProxyFinder = new IceProxyFinder<RobotStateComponentInterfacePrx>(this);

    pointCloudSegmenterProxyFinder->setSearchMask("*");
    primitiveExtractorProxyFinder->setSearchMask("*");
    affordanceExtractionProxyFinder->setSearchMask("*");
    pipelineVisualizationProxyFinder->setSearchMask("*");
    robotStateComponentProxyFinder->setSearchMask("*");

    ui->gridLayout->addWidget(pointCloudSegmenterProxyFinder, 3, 1, 1, 2);
    ui->gridLayout->addWidget(primitiveExtractorProxyFinder, 4, 1, 1, 2);
    ui->gridLayout->addWidget(affordanceExtractionProxyFinder, 5, 1, 1, 2);
    ui->gridLayout->addWidget(pipelineVisualizationProxyFinder, 6, 1, 1, 2);
    ui->gridLayout->addWidget(robotStateComponentProxyFinder, 7, 1, 1, 2);
}

AffordancePipelineGuiConfigDialog::~AffordancePipelineGuiConfigDialog()
{
    delete ui;
}

void AffordancePipelineGuiConfigDialog::onInitComponent()
{
    pointCloudSegmenterProxyFinder->setIceManager(getIceManager());
    primitiveExtractorProxyFinder->setIceManager(getIceManager());
    affordanceExtractionProxyFinder->setIceManager(getIceManager());
    pipelineVisualizationProxyFinder->setIceManager(getIceManager());
    robotStateComponentProxyFinder->setIceManager(getIceManager());
}

void AffordancePipelineGuiConfigDialog::onConnectComponent()
{
}

void AffordancePipelineGuiConfigDialog::onExitComponent()
{
    QObject::disconnect();
}

std::string AffordancePipelineGuiConfigDialog::getPlatform()
{
    return ui->lineEditPlatform->text().toStdString();
}

std::vector<std::string> AffordancePipelineGuiConfigDialog::getProviderNames()
{
    std::vector<std::string> result;
    std::string s = ui->lineEditProviderNames->text().toStdString();
    boost::split(result, s, boost::is_any_of(","));
    return result;
}

std::vector<std::string> AffordancePipelineGuiConfigDialog::getProviderDisplayNames()
{
    std::vector<std::string> result;
    std::string s = ui->lineEditProviderDisplayNames->text().toStdString();
    boost::split(result, s, boost::is_any_of(","));
    return result;
}

std::vector<std::string> AffordancePipelineGuiConfigDialog::getFilteredProviderNames()
{
    std::vector<std::string> result;
    std::string s = ui->lineEditFilteredProviderNames->text().toStdString();
    boost::split(result, s, boost::is_any_of(","));
    return result;
}

std::vector<std::string> AffordancePipelineGuiConfigDialog::getPipelineConfigurationFiles()
{
    std::vector<std::string> result;
    std::string s = ui->lineEditConfigFiles->text().toStdString();
    boost::split(result, s, boost::is_any_of(","));
    return result;
}

void AffordancePipelineGuiConfigDialog::verifyConfig()
{
    unsigned int l1 = pointCloudSegmenterProxyFinder->getSelectedProxyName().trimmed().length();
    unsigned int l2 = primitiveExtractorProxyFinder->getSelectedProxyName().trimmed().length();
    unsigned int l3 = affordanceExtractionProxyFinder->getSelectedProxyName().trimmed().length();
    unsigned int l4 = pipelineVisualizationProxyFinder->getSelectedProxyName().trimmed().length();
    unsigned int l5 = robotStateComponentProxyFinder->getSelectedProxyName().trimmed().length();

    if (!l1 || !l2 || !l3 || !l4 || !l5)
    {
        QMessageBox::critical(this, "Invalid Configuration", "The proxy names must not be empty");
        return;
    }

    if ((getProviderNames().size() != getProviderDisplayNames().size()) || (getProviderNames().size() != getFilteredProviderNames().size()))
    {
        QMessageBox::critical(this, "Invalid Configuration", "The provider name lists must be of equal size");
        return;
    }

    accept();
}
