/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

// PCL
#include <pcl/filters/filter.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

// Coin
#include <Inventor/nodes/SoSeparator.h>

#include <VisionX/interface/core/DataTypes.h>


namespace visionx
{
    class CoinPointCloud : public SoSeparator
    {
    public:
        using PointT = pcl::PointXYZRGBL;

    public:

        CoinPointCloud(pcl::PointCloud<PointT>::ConstPtr originalCloud,
                       visionx::PointContentType originalType,
                       bool colorFromLabel,
                       float pointSize = 1);
        ~CoinPointCloud() override;

        pcl::PointCloud<PointT>::ConstPtr getPCLCloud() const;
        visionx::PointContentType getOriginalType() const;


    private:

        pcl::PointCloud<PointT>::ConstPtr originalCloud;
        visionx::PointContentType originalType;

    };
}

