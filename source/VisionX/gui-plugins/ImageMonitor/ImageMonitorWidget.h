/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

/** ArmarX headers **/
#include <VisionX/gui-plugins/ImageMonitor/ui_ImageMonitorWidget.h>
#include <Image/ByteImage.h>
#include <IceUtil/Time.h>
namespace visionx
{
    class ImageMonitorWidgetController;
    class ImageMonitorPropertiesWidget;
    class ImageMonitorStatisticsWidget;
    class ImageViewerArea;

    class ImageMonitorWidget :
        public QWidget
    {

        Q_OBJECT

    public:
        ImageMonitorWidget(ImageMonitorWidgetController* controller);
        ~ImageMonitorWidget() override;

        // can be called from outside threads (e.g. controller)

        ImageViewerArea* getImageViewer() const;

    public slots:
        void drawImages(int numberImages, CByteImage** images, IceUtil::Time imageTimestamp, IceUtil::Time receiveTimestamp);
        void hideControlWidgets(bool hide = true);
        // external
        void setConnected(bool connected);
        void updateStatistics(const QString& statisticsStr);
        void updateRecordButton(bool);

        // internal
        void settingsButtonClicked(bool toggled);
        void statisticsButtonClicked(bool toggled);
        void snapshotButtonClicked(bool toggled);
        void playButtonToggled(bool toggled);
        void propertiesAccepted();
        void statisticsAccepted();
        void recordButtonToggled(bool toggled);
        void bufferImagesToggled(bool toggled);
        void sliderPositionChanged(int pos);
        void bufferImagesPaneChanged(bool toggled);

    signals:
        // pass

    private:
        Ui::ImageMonitor ui;
        ImageViewerArea* imageViewer;
        ImageMonitorWidgetController* controller;
        ImageMonitorPropertiesWidget* imageMonitorPropertiesWidget;
        ImageMonitorStatisticsWidget* imageMonitorStatisticsWidget;

    };
}


