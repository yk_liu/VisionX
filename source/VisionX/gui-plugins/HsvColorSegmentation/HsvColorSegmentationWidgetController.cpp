/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    VisionX::gui-plugins::HsvColorSegmentationWidgetController
 * \author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * \date       2018
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HsvColorSegmentationWidgetController.h"

#include <pwd.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <QFileDialog>
#include <QMessageBox>

// ivt
#include <Image/PrimitivesDrawer.h>

#include <VisionX/interface/core/ImageProviderInterface.h>
#include <VisionX/tools/TypeMapping.h>


using namespace armarx;


const QStringList HsvColorSegmentationWidgetController::COLOR_PARAMETER_NAMES =
{
    "none", "blue", "blue2", "blue3", "colored", "green", "green2", "green3",
    "orange", "orange2", "orange3", "red", "red2", "red3", "skin", "white",
    "yellow", "yellow2", "yellow3"
};

const std::string HsvColorSegmentationWidgetController::DEFAULT_COLOR_PARAMETER_FILE =
    // "armarx/VisionX/data/VisionX/examples/colors.txt";
    "armarx/VisionX/data/VisionX/examples/cebit-colors.txt";



HsvColorSegmentationWidgetController::HsvColorSegmentationWidgetController()
{
    widget.setupUi(getWidget());

    widget.comboBoxColor->addItems(COLOR_PARAMETER_NAMES);
    widget.comboBoxColor->setCurrentIndex(0);
    currentColorIndex = 0;

    autoValues.resize(NUM_PARAMS, 0);


    guiAutoCheckBoxes.resize(NUM_PARAMS);
    guiAutoCheckBoxes[HUE_MID] = widget.checkBoxAutoHue;
    guiAutoCheckBoxes[HUE_TOL] = widget.checkBoxAutoHueTol;
    guiAutoCheckBoxes[SAT_MIN] = widget.checkBoxAutoSatMin;
    guiAutoCheckBoxes[SAT_MAX] = widget.checkBoxAutoSatMax;
    guiAutoCheckBoxes[VAL_MIN] = widget.checkBoxAutoValMin;
    guiAutoCheckBoxes[VAL_MAX] = widget.checkBoxAutoValMax;

    guiValueSpinBoxes.resize(NUM_PARAMS);
    guiValueSpinBoxes[HUE_MID] = widget.spinBoxHue;
    guiValueSpinBoxes[HUE_TOL] = widget.spinBoxHueTol;
    guiValueSpinBoxes[SAT_MIN] = widget.spinBoxSatMin;
    guiValueSpinBoxes[SAT_MAX] = widget.spinBoxSatMax;
    guiValueSpinBoxes[VAL_MIN] = widget.spinBoxValMin;
    guiValueSpinBoxes[VAL_MAX] = widget.spinBoxValMax;

    guiValueSliders.resize(NUM_PARAMS);
    guiValueSliders[HUE_MID] = widget.sliderHue;
    guiValueSliders[HUE_TOL] = widget.sliderHueTol;
    guiValueSliders[SAT_MIN] = widget.sliderSatMin;
    guiValueSliders[SAT_MAX] = widget.sliderSatMax;
    guiValueSliders[VAL_MIN] = widget.sliderValMin;
    guiValueSliders[VAL_MAX] = widget.sliderValMax;


    // proxy finder
    proxyFinder = new armarx::IceProxyFinder<visionx::ImageProviderInterfacePrx>();
    proxyFinder->setSearchMask("*Provider|*Result");
    widget.layoutChooseProvider->addWidget(proxyFinder);


    // image viewer input
    imageViewerInput = new SelectableImageViewer();
    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    imageViewerInput->setSizePolicy(sizePolicy);

    widget.layoutImageViewerInput->addWidget(imageViewerInput);
    imageViewerInput->show();


    // image viewer output
    imageViewerOutput = new visionx::ImageViewerArea();
    //QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    imageViewerOutput->setSizePolicy(sizePolicy);

    widget.layoutImageViewerOutput->addWidget(imageViewerOutput);
    imageViewerOutput->show();


    armarx::CMakePackageFinder finder("VisionX");
    if (finder.packageFound())
    {
        ARMARX_INFO << "Adding to datapaths: " << finder.getDataDir();
        armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());
    }
}


HsvColorSegmentationWidgetController::~HsvColorSegmentationWidgetController()
{

}


void HsvColorSegmentationWidgetController::loadSettings(QSettings* settings)
{
    widget.lineEditColorParameterFile->setText(settings->value("ColorParameterFile", getDefaultColorParameterFile()).toString());
    loadColorParameterSetFromFile();
}

void HsvColorSegmentationWidgetController::saveSettings(QSettings* settings)
{
    settings->setValue("ColorParameterFile", widget.lineEditColorParameterFile->text());
}


void HsvColorSegmentationWidgetController::onInitImageProcessor()
{
    proxyFinder->setIceManager(this->getIceManager(), proxyFinder->getSelectedProxyName().isEmpty());

    connected = false;
    isPaused = true;

    connect(widget.buttonConnect, SIGNAL(clicked(bool)), this, SLOT(clickedButtonConnect(bool)));
    connect(widget.buttonDisconnect, SIGNAL(clicked(bool)), this, SLOT(clickedButtonDisconnect(bool)));
    connect(widget.buttonPlayPause, SIGNAL(clicked(bool)), this, SLOT(clickedButtonPlayPause(bool)));

    // image viewer selection
    connect(imageViewerInput, SIGNAL(selected(int, float, float)), this, SLOT(addColorSelection(int, float, float)));
    connect(widget.buttonClearSelectedPoints, SIGNAL(pressed()), this, SLOT(clearColorSelection()));

    // COLOR SETTINGS

    connect(widget.buttonColorSettingsReset, SIGNAL(pressed()), this, SLOT(resetCurrentColorSettings()));

    // sliders to manual setters
    connect(widget.sliderHue, SIGNAL(valueChanged(int)), this, SLOT(setManualHueMid(int)));
    connect(widget.sliderHueTol, SIGNAL(valueChanged(int)), this, SLOT(setManualHueTol(int)));
    connect(widget.sliderSatMin, SIGNAL(valueChanged(int)), this, SLOT(setManualSatMin(int)));
    connect(widget.sliderSatMax, SIGNAL(valueChanged(int)), this, SLOT(setManualSatMax(int)));
    connect(widget.sliderValMin, SIGNAL(valueChanged(int)), this, SLOT(setManualValMin(int)));
    connect(widget.sliderValMax, SIGNAL(valueChanged(int)), this, SLOT(setManualValMax(int)));

    // spin boxes to manual setters
    connect(widget.spinBoxHue, SIGNAL(valueChanged(int)), this, SLOT(setManualHueMid(int)));
    connect(widget.spinBoxHueTol, SIGNAL(valueChanged(int)), this, SLOT(setManualHueTol(int)));
    connect(widget.spinBoxSatMin, SIGNAL(valueChanged(int)), this, SLOT(setManualSatMin(int)));
    connect(widget.spinBoxSatMax, SIGNAL(valueChanged(int)), this, SLOT(setManualSatMax(int)));
    connect(widget.spinBoxValMin, SIGNAL(valueChanged(int)), this, SLOT(setManualValMin(int)));
    connect(widget.spinBoxValMax, SIGNAL(valueChanged(int)), this, SLOT(setManualValMax(int)));

    // check boxes to auto setters
    connect(widget.checkBoxAutoAll, SIGNAL(toggled(bool)), this, SLOT(onToggledCheckBoxAutoAll(bool)));
    connect(widget.checkBoxAutoHue, SIGNAL(toggled(bool)), this, SLOT(onToggledCheckBoxAuto(bool)));
    connect(widget.checkBoxAutoHueTol, SIGNAL(toggled(bool)), this, SLOT(onToggledCheckBoxAuto(bool)));
    connect(widget.checkBoxAutoSatMin, SIGNAL(toggled(bool)), this, SLOT(onToggledCheckBoxAuto(bool)));
    connect(widget.checkBoxAutoSatMax, SIGNAL(toggled(bool)), this, SLOT(onToggledCheckBoxAuto(bool)));
    connect(widget.checkBoxAutoValMin, SIGNAL(toggled(bool)), this, SLOT(onToggledCheckBoxAuto(bool)));
    connect(widget.checkBoxAutoValMax, SIGNAL(toggled(bool)), this, SLOT(onToggledCheckBoxAuto(bool)));

    connect(widget.spinBoxAddTol, SIGNAL(valueChanged(int)), this, SLOT(onChangedAddTolerance(int)));


    // color selection

    connect(widget.comboBoxColor, SIGNAL(currentIndexChanged(int)), this, SLOT(onColorIndexChanged(int)));
    connect(widget.buttonColorParameterFileSearch, SIGNAL(pressed()), this, SLOT(searchColorParameterFile()));
    connect(widget.buttonColorParameterFileReset, SIGNAL(pressed()), this, SLOT(resetColorParameterFile()));
    connect(widget.buttonColorParametersReload, SIGNAL(pressed()), this, SLOT(loadColorParameterSetFromFile()));
    connect(widget.buttonColorParametersSave, SIGNAL(pressed()), this, SLOT(saveColorParameterSetToFile()));


    // inner connections
    connect(this, SIGNAL(imageProviderConnected(bool)), this, SLOT(updateImageMonitorUI()));


    loadColorParameterSetFromFile(false);
    resetCurrentColorSettings();
}

void HsvColorSegmentationWidgetController::onConnectImageProcessor()
{

}

void HsvColorSegmentationWidgetController::onDisconnectImageProcessor()
{

}

void HsvColorSegmentationWidgetController::onExitImageProcessor()
{

}


void HsvColorSegmentationWidgetController::reconnect()
{
    armarx::ScopedRecursiveLock lock(imageMutex);

    disconnectFromProvider();

    // connect to provider
    try
    {
        connectToProvider();
    }
    catch (...)
    {
        armarx::handleExceptions();
    }
}

void HsvColorSegmentationWidgetController::connectToProvider()
{
    if (connected)
    {
        return;
    }

    armarx::ScopedRecursiveLock lock(imageMutex);

    imageProviderName = proxyFinder->getSelectedProxyName().toStdString();
    ARMARX_INFO << "Trying to connect to provider " << imageProviderName;

    usingImageProvider(imageProviderName);

    ARMARX_INFO << getName() << " connecting to " << imageProviderName;

    // connect to proxy
    imageProviderInfo = getImageProvider(imageProviderName, visionx::tools::typeNameToImageType("rgb"));

    int width = imageProviderInfo.imageFormat.dimension.width;
    int height = imageProviderInfo.imageFormat.dimension.height;

    segmentation = HsvImageSegmentation(imageProviderInfo);

    bool makeResultImageNamePretty = false;
    std::string managedIceObjectName;
    if (makeResultImageNamePretty)
    {
        managedIceObjectName = getName();
        setName(HsvColorSegmentationWidgetController::GetWidgetName().toStdString());
    }

    ARMARX_INFO << "Enabling result images";
    enableResultImages(numImages, visionx::ImageDimension(width, height), visionx::ImageType::eRgb);

    if (makeResultImageNamePretty)
    {
        setName(managedIceObjectName);
    }

    isPaused = false;
    connected = true;

    emit imageProviderConnected(true);
}

void HsvColorSegmentationWidgetController::disconnectFromProvider()
{
    if (!connected)
    {
        return;
    }

    ARMARX_WARNING << "ImageProcessor does not allow re-enabling result images. "
                   "If you connect to another image provider, there might be "
                   "errors in the result images due to different image formats.";

    ARMARX_INFO << "Disconnecting from provider " << imageProviderName;

    armarx::ScopedRecursiveLock lock(imageMutex);
    segmentation = HsvImageSegmentation();

    releaseImageProvider(imageProviderName);

    imageProviderPrx = 0;

    connected = false;
    isPaused = true;

    emit imageProviderConnected(false);
}


void HsvColorSegmentationWidgetController::process()
{
    armarx::ScopedRecursiveLock lock(imageMutex);

    if (!lock.owns_lock())
    {
        ARMARX_INFO << deactivateSpam(3) << "unable to process next image(" << lock.owns_lock() << ")";
        return;
    }

    if (!connected)
    {
        ARMARX_INFO << deactivateSpam(3) << "Not connected to any image provider";
        return;
    }

    armarx::MetaInfoSizeBasePtr info;
    IceUtil::Time timeReceived;
    if (!isPaused)
    {
        //only update current image if unpaused
        if (!waitForImages(imageProviderName))
        {
            ARMARX_WARNING << "Timeout while waiting for images";
            return;
        }

        if (getImages(imageProviderName, segmentation.getInputImagesRgb(), info) != numImages)
        {
            ARMARX_WARNING << "Unable to transfer or read images";
            return;
        }
        timeReceived = TimeUtil::GetTime();
    }

    // perform segmentation
    segmentation.processInputImages(value(HUE_MID), value(HUE_TOL), value(SAT_MIN),
                                    value(SAT_MAX), value(VAL_MIN), value(VAL_MAX));

    // draw selected points on current input images
    drawSelectedPoints();

    // update image viewers
    imageViewerInput->setImages(numImages, segmentation.getInputVisuImages(), IceUtil::Time::microSeconds(info->timeProvided), timeReceived);
    imageViewerOutput->setImages(numImages, segmentation.getOutputImagesRgb(), IceUtil::Time::microSeconds(info->timeProvided), timeReceived);

    // publish results
    provideResultImages(segmentation.getOutputImagesRgb());
}




void HsvColorSegmentationWidgetController::clickedButtonConnect(bool toggled)
{
    reconnect();
}

void HsvColorSegmentationWidgetController::clickedButtonDisconnect(bool toggled)
{
    disconnectFromProvider();
}

void HsvColorSegmentationWidgetController::clickedButtonPlayPause(bool toggled)
{
    if (!connected)
    {
        isPaused = false;
    }
    else
    {
        isPaused = !isPaused;
    }
    updateImageMonitorUI();
}

void HsvColorSegmentationWidgetController::addColorSelection(int imageIndex, float x, float y)
{
    if (!connected)
    {
        return;
    }

    // lookup pixel value in respective image

    int red, green, blue;
    {
        armarx::ScopedRecursiveLock lock(imageMutex);

        CByteImage* image = segmentation.getInputImagesRgb()[imageIndex];
        int col = image->width * x;
        int row = image->height * y;

        if (image->type != CByteImage::eRGB24)
        {
            ARMARX_WARNING << "Input image type is not eRGB24 (is " << image->type
                           << " instead of " << CByteImage::eRGB24 << ")";
        }

        int pixelIndex = 0;
        pixelIndex += row * image->width * image->bytesPerPixel; // skip rows
        pixelIndex += col * image->bytesPerPixel; // skip columns in row

        red = image->pixels[pixelIndex];
        green = image->pixels[pixelIndex + 1];
        blue = image->pixels[pixelIndex + 2];
    }


    // convert to HSV and store point

    SelectedPoint sp(imageIndex, x, y, red, green, blue);

    selectedPoints.push_back(sp);

    // update
    onSelectedPointsChanged();
}


void HsvColorSegmentationWidgetController::clearColorSelection()
{
    selectedPoints.clear();
    onSelectedPointsChanged();
}

void HsvColorSegmentationWidgetController::onColorSettingsChanged()
{
    if (dirtyColors.find(widget.comboBoxColor->currentIndex()) == dirtyColors.end())
    {
        markCurrentColorDirty();
    }

    updateColorVisualization();

    widget.buttonColorParametersSave->setText("Save");
}

void HsvColorSegmentationWidgetController::onColorIndexChanged(int value)
{
    saveCurrentColorParameters(currentColorIndex);
    loadCurrentColorParameters(value);
    currentColorIndex = value;
}


void HsvColorSegmentationWidgetController::setManualHueMid(int value)
{
    guiAutoCheckBoxes[HUE_MID]->setChecked(false);
    onColorSettingsChanged();
}

void HsvColorSegmentationWidgetController::setManualHueTol(int value)
{
    guiAutoCheckBoxes[HUE_TOL]->setChecked(false);
    onColorSettingsChanged();
}

void HsvColorSegmentationWidgetController::setManualSatMin(int value)
{
    guiAutoCheckBoxes[SAT_MIN]->setChecked(false);
    onColorSettingsChanged();
}

void HsvColorSegmentationWidgetController::setManualSatMax(int value)
{
    guiAutoCheckBoxes[SAT_MAX]->setChecked(false);
    onColorSettingsChanged();
}

void HsvColorSegmentationWidgetController::setManualValMin(int value)
{
    guiAutoCheckBoxes[VAL_MIN]->setChecked(false);
    onColorSettingsChanged();
}

void HsvColorSegmentationWidgetController::setManualValMax(int value)
{
    guiAutoCheckBoxes[VAL_MAX]->setChecked(false);
    onColorSettingsChanged();
}

void HsvColorSegmentationWidgetController::onToggledCheckBoxAutoAll(bool enabled)
{
    widget.checkBoxAutoHue->setChecked(enabled);
    widget.checkBoxAutoHueTol->setChecked(enabled);
    widget.checkBoxAutoSatMin->setChecked(enabled);
    widget.checkBoxAutoSatMax->setChecked(enabled);
    widget.checkBoxAutoValMin->setChecked(enabled);
    widget.checkBoxAutoValMax->setChecked(enabled);

    onToggledCheckBoxAuto(enabled);
}

void HsvColorSegmentationWidgetController::onToggledCheckBoxAuto(bool enabled)
{
    if (enabled)
    {
        updateAutoValuesUI();
        onColorSettingsChanged();
    }
}

void HsvColorSegmentationWidgetController::onChangedAddTolerance(int value)
{
    recomputeAutoValues();
    updateAutoValuesUI();
}

void HsvColorSegmentationWidgetController::resetCurrentColorSettings()
{
    loadCurrentColorParameters(widget.comboBoxColor->currentIndex());
}

void HsvColorSegmentationWidgetController::loadColorParameterSetFromFile(bool showMsgBoxes)
{
    if (showMsgBoxes && !dirtyColors.empty())
    {
        QMessageBox msgBox;
        msgBox.setText("Unsaved changes");
        std::stringstream infText;
        infText << "There are colors with unsaved changes: \n\n";
        for (int i : dirtyColors)
        {
            infText << COLOR_PARAMETER_NAMES.at(i).toStdString() << " ";
        }
        infText << "\n\nAre you sure to discard these changes?";

        msgBox.setInformativeText(QString::fromStdString(infText.str()));
        msgBox.setIcon(QMessageBox::Question);
        msgBox.setStandardButtons(QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Discard);
        int option = msgBox.exec();
        if (option == QMessageBox::Cancel)
        {
            return; // cancel
        }
    }

    std::string fileName = colorParameterFile();
    if (!ArmarXDataPath::getAbsolutePath(fileName, fileName))
    {
        ARMARX_WARNING << "Could not find color parameter file in ArmarXDataPath: " << fileName;
    }

    bool success = colorParameterSet.LoadFromFile(fileName.c_str());
    if (!success)
    {
        if (showMsgBoxes)
        {
            std::stringstream text;
            text << "Failed to load color parameters from file:\n\n"
                 << "" << fileName << "\n\n"
                 << "Please check whether the file exists and is readable.";

            QMessageBox msgBox;
            msgBox.setText("Could not load color parameters.");
            msgBox.setInformativeText(QString::fromStdString(text.str()));
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
        }


        return;
    }

    widget.lineEditColorParameterFile->setText(QString::fromStdString(fileName));
    loadCurrentColorParameters(widget.comboBoxColor->currentIndex());

    markAllColorsClean();
}

void HsvColorSegmentationWidgetController::saveColorParameterSetToFile()
{
    saveCurrentColorParameters(widget.comboBoxColor->currentIndex());

    bool success = colorParameterSet.SaveToFile(colorParameterFile().c_str());
    if (!success)
    {
        std::stringstream text;
        text << "Failed to save color parameters to file:\n\n"
             << "" << colorParameterFile() << "\n\n"
             << "Please check whether the file is writable.";

        QMessageBox msgBox;
        msgBox.setText("Could not save color parameters.");
        msgBox.setInformativeText(QString::fromStdString(text.str()));
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();

        return;
    }

    markAllColorsClean();
    widget.buttonColorParametersSave->setText("Saved");
}

void HsvColorSegmentationWidgetController::searchColorParameterFile()
{
    QString selectedFile = QFileDialog::getOpenFileName(
                               widget.buttonColorParameterFileSearch,
                               tr("Choose Color Parameter File"),
                               widget.lineEditColorParameterFile->text());

    if (!selectedFile.isNull())
    {
        widget.lineEditColorParameterFile->setText(selectedFile);
    }
}

void HsvColorSegmentationWidgetController::resetColorParameterFile()
{
    widget.lineEditColorParameterFile->setText(getDefaultColorParameterFile());
}


int HsvColorSegmentationWidgetController::value(int param)
{
    return guiValueSpinBoxes[param]->value();
}

int HsvColorSegmentationWidgetController::hueMin()
{
    return std::max(0, value(HUE_MID) - value(HUE_TOL));
}

int HsvColorSegmentationWidgetController::hueMax()
{
    return std::min(179, value(HUE_MID) + value(HUE_TOL));
}

int HsvColorSegmentationWidgetController::satMid()
{
    return 0.5 * (value(SAT_MIN) + value(SAT_MAX));
}

int HsvColorSegmentationWidgetController::valMid()
{
    return 0.5 * (value(VAL_MIN) + value(VAL_MAX));
}

bool HsvColorSegmentationWidgetController::isAutoEnabled(int param)
{
    return guiAutoCheckBoxes[param]->isChecked();
}

int HsvColorSegmentationWidgetController::additionalTolerance()
{
    return widget.spinBoxAddTol->value();
}

std::string HsvColorSegmentationWidgetController::colorParameterFile()
{
    return widget.lineEditColorParameterFile->text().toStdString();
}

void HsvColorSegmentationWidgetController::recomputeAutoValues()
{
    for (int i = 0; i < NUM_PARAMS; ++i)
    {
        autoValues[i] = 0;
    }

    if (selectedPoints.empty())
    {
        return; // all 0
    }

    // compute minima and maxima

    // trigger min() in first element by setting mins to 255
    int hueMin = 179;
    int hueMax = 0;
    autoValues[SAT_MIN] = 255;
    autoValues[VAL_MIN] = 255;

    auto setMin = [](int& target, int compare)
    {
        target = std::min(target, compare);
    };
    auto setMax = [](int& target, int compare)
    {
        target = std::max(target, compare);
    };

    for (SelectedPoint& p : selectedPoints)
    {
        setMin(hueMin, p.hue);
        setMax(hueMax, p.hue);
        setMin(autoValues[SAT_MIN], p.sat);
        setMax(autoValues[SAT_MAX], p.sat);
        setMin(autoValues[VAL_MIN], p.val);
        setMax(autoValues[VAL_MAX], p.val);
    }

    // correct hue mid and tol

    int hueMid = (hueMin + hueMax) / 2;
    int hueTol = std::max(hueMax - hueMid, hueMid - hueMin);

    autoValues[HUE_MID] = hueMid;
    autoValues[HUE_TOL] = hueTol;


    // add / subtract additional tolerance
    int tolerance = additionalTolerance();

    auto clampedAdd = [](int& target, int addSub, int maxMin)
    {
        if (addSub > 0)
        {
            target = std::min(target + addSub, maxMin);
        }
        else if (addSub < 0)
        {
            target = std::max(target + addSub, maxMin);
        }
    };

    clampedAdd(autoValues[HUE_TOL], 2 * tolerance, 179);

    clampedAdd(autoValues[SAT_MIN], - tolerance, 0);
    clampedAdd(autoValues[SAT_MAX], tolerance, 255);

    clampedAdd(autoValues[VAL_MIN], - tolerance, 0);
    clampedAdd(autoValues[VAL_MAX], tolerance, 255);

}

void HsvColorSegmentationWidgetController::onSelectedPointsChanged()
{
    recomputeAutoValues();
    updateAutoValuesUI();

    std::stringstream num;
    num << selectedPoints.size();
    widget.labelSelectedPointsNum->setText(QString(num.str().c_str()));

    onColorSettingsChanged();
}

void HsvColorSegmentationWidgetController::drawSelectedPoints()
{
    CByteImage** inputVisuImages = segmentation.getInputVisuImages();

    int radius = 20;
    for (SelectedPoint& sp : selectedPoints)
    {
        CByteImage* image = inputVisuImages[sp.imageIndex];
        PrimitivesDrawer::DrawCircle(
            image, sp.xRel * image->width, sp.yRel * image->height, radius,
            sp.red, sp.green, sp.blue, -1); // fill

        PrimitivesDrawer::DrawCircle(
            image, sp.xRel * image->width, sp.yRel * image->height, radius,
            0, 0, 0, 2); // border
    }
}


void HsvColorSegmentationWidgetController::saveCurrentColorParameters(int colorIndex)
{
    std::string colorName = COLOR_PARAMETER_NAMES.at(colorIndex).toStdString();

    ObjectColor objectColor = CColorParameterSet::Translate(colorName.c_str());

    colorParameterSet.SetColorParameters(objectColor,
                                         value(HUE_MID), value(HUE_TOL),
                                         value(SAT_MIN), value(SAT_MAX),
                                         value(VAL_MIN), value(VAL_MAX));
}

void HsvColorSegmentationWidgetController::loadCurrentColorParameters(int colorIndex)
{
    // setting the new values will cause the new color to be marked dirty
    // we have to check beforehand and restore the marker afterwards
    bool isDirty = dirtyColors.find(colorIndex) != dirtyColors.end();

    std::string colorName = COLOR_PARAMETER_NAMES.at(colorIndex).toStdString();

    ObjectColor objectColor = CColorParameterSet::Translate(colorName.c_str());

    const int* params = colorParameterSet.GetColorParameters(objectColor);

    widget.spinBoxHue->setValue(params[0]);
    widget.spinBoxHueTol->setValue(params[1]);
    widget.spinBoxSatMin->setValue(params[2]);
    widget.spinBoxSatMax->setValue(params[3]);
    widget.spinBoxValMin->setValue(params[4]);
    widget.spinBoxValMax->setValue(params[5]);

    widget.checkBoxAutoAll->setChecked(false);

    if (isDirty)
    {
        markCurrentColorDirty();
    }
    else
    {
        markCurrentColorClean();
    }
}


void HsvColorSegmentationWidgetController::updatePausePlayButtonText()
{
    const char* text = isPaused ? "Play" : "Pause";
    widget.buttonPlayPause->setText(text);
}

void HsvColorSegmentationWidgetController::updateImageMonitorUI()
{
    updatePausePlayButtonText();

    widget.buttonDisconnect->setEnabled(connected);
    widget.buttonPlayPause->setEnabled(connected);

    QString statusString;
    if (connected)
    {
        std::stringstream ss;
        ss << "Status: connected to image provider \n" << imageProviderName;
        ss << " (" << numImages << " imgs, " << imageWidth << "x" << imageHeight << ")";

        if (isPaused)
        {
            ss << " - PAUSED";
        }
        statusString = QString::fromStdString(ss.str());
    }
    else
    {
        statusString = "Status: not connected";
    }
    widget.labelImageMonitorStatus->setText(statusString);
}

void HsvColorSegmentationWidgetController::setUiParamValue(int param, int value)
{
    guiValueSpinBoxes[param]->setValue(value);
    guiValueSliders[param]->setValue(value);
}

void HsvColorSegmentationWidgetController::updateAutoValuesUI()
{
    for (int i = 0; i < NUM_PARAMS; ++i)
    {
        if (isAutoEnabled(i))
        {
            setUiParamValue(i, autoValues[i]);
            guiAutoCheckBoxes[i]->setChecked(true);
        }
    }
}

void HsvColorSegmentationWidgetController::updateColorVisualization()
{
    // mid color preview

    QPalette previewPalette = widget.labelColorPreview->palette();
    previewPalette.setColor(QPalette::Background, QColor::fromHsv(value(HUE_MID), satMid(), valMid()));
    widget.labelColorPreview->setPalette(previewPalette);


    // gradients

    // create gradians
    QLinearGradient gradHue = QLinearGradient(0, 0, 0, widget.labelGradHue->height());
    QLinearGradient gradSat = QLinearGradient(0, 0, 0, widget.labelGradSat->height());
    QLinearGradient gradVal = QLinearGradient(0, 0, 0, widget.labelGradVal->height());

    // set gradians
    int numHueSamples = 10;
    int hMin = hueMin();
    int hMax = hueMax();
    for (int i = 0; i <= numHueSamples; ++i)
    {
        float interpol = float(i) / numHueSamples;
        int value = int(hMin * (1 - interpol) + hMax * interpol);
        // Qt uses a hue range of 0-359
        gradHue.setColorAt(interpol, QColor::fromHsv(2 * value, 255, 255));
    }

    gradSat.setColorAt(0, QColor::fromHsv(value(HUE_MID), value(SAT_MIN), 255));
    gradSat.setColorAt(1, QColor::fromHsv(value(HUE_MID), value(SAT_MAX), 255));

    gradVal.setColorAt(0, QColor::fromHsv(0, 0, value(VAL_MIN)));
    gradVal.setColorAt(1, QColor::fromHsv(0, 0, value(VAL_MAX)));


    // assignGradients
    auto setGradient = [](QLabel * label, const QLinearGradient & gradient)
    {
        QPalette palette = label->palette();
        palette.setBrush(QPalette::Background, gradient);
        label->setPalette(palette);
    };

    setGradient(widget.labelGradHue, gradHue);
    setGradient(widget.labelGradSat, gradSat);
    setGradient(widget.labelGradVal, gradVal);
}

void HsvColorSegmentationWidgetController::markCurrentColorDirty()
{
    int idx = widget.comboBoxColor->currentIndex();
    QString newText = COLOR_PARAMETER_NAMES.at(idx) +  "*";
    widget.comboBoxColor->setItemText(idx, newText);

    dirtyColors.insert(idx);
}

void HsvColorSegmentationWidgetController::markCurrentColorClean()
{
    int idx = widget.comboBoxColor->currentIndex();
    widget.comboBoxColor->setItemText(idx, COLOR_PARAMETER_NAMES.at(idx));
    dirtyColors.erase(idx);
}

void HsvColorSegmentationWidgetController::markAllColorsClean()
{
    for (int i = 0; i < widget.comboBoxColor->count(); ++i)
    {
        widget.comboBoxColor->setItemText(i, COLOR_PARAMETER_NAMES.at(i));
    }
    dirtyColors.clear();
}

void HsvColorSegmentationWidgetController::rgbToHsv(int r, int g, int b, int& h, int& s, int& v)
{
    // let ivt convert an 1x1 pixel image...

    CByteImage rgb(1, 1, CByteImage::eRGB24);
    CByteImage hsv(1, 1, CByteImage::eRGB24);

    rgb.pixels[0] = r;
    rgb.pixels[1] = g;
    rgb.pixels[2] = b;

    ::ImageProcessor::CalculateHSVImage(&rgb, &hsv);

    h = hsv.pixels[0];
    s = hsv.pixels[1];
    v = hsv.pixels[2];
}

std::string HsvColorSegmentationWidgetController::getHomeDirectory()
{
    // check HOME environment variable
    char* homeDir = getenv("HOME");

    if (!homeDir)
    {
        // HOME not set
        passwd* pw = getpwuid(getuid());
        homeDir = pw->pw_dir;
    }

    return std::string(homeDir);
}

QString HsvColorSegmentationWidgetController::getDefaultColorParameterFile()
{
    std::stringstream path;
    path << getHomeDirectory();
    path << "/" << DEFAULT_COLOR_PARAMETER_FILE;

    return QString::fromStdString(path.str());
}


