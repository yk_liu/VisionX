#include "HsvImageSegmentation.h"

#include <VisionX/tools/ImageUtil.h>


using namespace armarx;


HsvImageSegmentation::HsvImageSegmentation()
{

}

HsvImageSegmentation::HsvImageSegmentation(
    const visionx::ImageProviderInfo& imageProviderInfo) :
    numImages(imageProviderInfo.numberImages),
    width(imageProviderInfo.imageFormat.dimension.width),
    height(imageProviderInfo.imageFormat.dimension.height)
{
    if (numImages <= 0)
    {
        std::stringstream ss;
        ss << "Tried to initialize HsvImageSegmentation with " << numImages
           << " images, but need at least 1 image.";
        throw std::invalid_argument(ss.str());
    }

    allocate(imageProviderInfo);
}

HsvImageSegmentation::~HsvImageSegmentation()
{
    deallocate();
}

HsvImageSegmentation::HsvImageSegmentation(HsvImageSegmentation&& other)
{
    moveFrom(other);
}

HsvImageSegmentation& HsvImageSegmentation::operator =(HsvImageSegmentation&& other)
{
    if (this != &other)
    {
        deallocate();
        moveFrom(other);
    }
    return *this;
}

CByteImage** HsvImageSegmentation::getInputImagesRgb() const
{
    return inputImagesRgb;
}

CByteImage** HsvImageSegmentation::getInputImagesHsv() const
{
    return inputImagesHsv;
}

CByteImage** HsvImageSegmentation::getInputVisuImages() const
{
    return inputVisuImages;
}

CByteImage** HsvImageSegmentation::getOutputImagesGray() const
{
    return outputImagesGray;
}

CByteImage** HsvImageSegmentation::getOutputImagesRgb() const
{
    return outputImagesRgb;
}

int HsvImageSegmentation::getNumImages() const
{
    return numImages;
}

void HsvImageSegmentation::processInputImages(int hue, int hueTol, int satMin, int satMax, int valMin, int valMax)
{
    // segment all input images
    // #pragma omp for // seems to get ignored
    for (int i = 0; i < numImages; ++i)
    {
        ::ImageProcessor::CopyImage(inputImagesRgb[i], inputVisuImages[i]);

        // convert to HSV
        ::ImageProcessor::CalculateHSVImage(inputImagesRgb[i], inputImagesHsv[i]);

        // filter HSV (-> gray scale image)
        ::ImageProcessor::FilterHSV(inputImagesHsv[i], outputImagesGray[i],
                                    hue, hueTol, satMin, satMax, valMin, valMax);

        // convert gray scale to RGB
        ::ImageProcessor::ConvertImage(outputImagesGray[i], outputImagesRgb[i], true);
    }
}

void HsvImageSegmentation::allocate(const visionx::ImageProviderInfo& imageProviderInfo)
{
    inputImagesRgb = new CByteImage*[numImages];
    inputVisuImages = new CByteImage*[numImages];
    inputImagesHsv = new CByteImage*[numImages];
    outputImagesGray = new CByteImage*[numImages];
    outputImagesRgb = new CByteImage*[numImages];

    for (int i = 0; i < numImages; ++i)
    {
        inputImagesRgb[i] = visionx::tools::createByteImage(imageProviderInfo);
        inputVisuImages[i] = visionx::tools::createByteImage(imageProviderInfo);
        inputImagesHsv[i] = visionx::tools::createByteImage(imageProviderInfo);
        outputImagesGray[i] = new CByteImage(width, height, CByteImage::eGrayScale);
        outputImagesRgb[i] = visionx::tools::createByteImage(imageProviderInfo);
    }
}

void HsvImageSegmentation::deallocate()
{
    if (numImages != 0)
    {
        for (int i = 0; i < numImages; ++i)
        {
            delete inputImagesRgb[i];
            delete inputVisuImages[i];
            delete inputImagesHsv[i];
            delete outputImagesGray[i];
            delete outputImagesRgb[i];
        }

        delete[] inputImagesRgb;
        delete[] inputVisuImages;
        delete[] inputImagesHsv;
        delete[] outputImagesGray;
        delete[] outputImagesRgb;

        inputImagesRgb = nullptr;
        inputVisuImages = nullptr;
        inputImagesHsv = nullptr;
        outputImagesGray = nullptr;
        outputImagesRgb = nullptr;
    }
}

void HsvImageSegmentation::moveFrom(HsvImageSegmentation& other)
{
    this->numImages = other.numImages;
    this->height = other.height;
    this->width = other.width;

    // steal image buffers
    this->inputImagesRgb = other.inputImagesRgb;
    this->inputVisuImages = other.inputVisuImages;
    this->inputImagesHsv = other.inputImagesHsv;
    this->outputImagesGray = other.outputImagesGray;
    this->outputImagesRgb = other.outputImagesRgb;

    // reset others state to default
    other.numImages = other.height = other.width = 0;
    other.inputImagesRgb = other.inputImagesHsv = other.inputVisuImages = nullptr;
    other.outputImagesGray = other.outputImagesRgb = nullptr;
}
