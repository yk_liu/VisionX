/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ObjectRecognitionTest::ArmarXObjects::RequestClass
 * @author     welke ( welke at kit dot edu )
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RequestClass.h"
#include <ArmarXCore/observers/condition/Term.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>

using namespace armarx;
using namespace memoryx;



void RequestClass::onInitComponent()
{
    usingProxy("ObjectMemoryObserver");
    usingProxy("ConditionHandler");

}


void RequestClass::onConnectComponent()
{
    // retrieve proxies
    observer = getProxy<memoryx::ObjectMemoryObserverInterfacePrx>("ObjectMemoryObserver");
    conditionHandler = getProxy<ConditionHandlerInterfacePrx>("ConditionHandler");

    ARMARX_INFO_S << "Waiting a bit ..." << std::endl;

    sleep(5);

    //    ARMARX_INFO_S << "=============================================================";
    //    ARMARX_INFO_S << " USECASE I: query a specific object and wait until localized";
    //    ARMARX_INFO_S << "=============================================================";
    //    LocalizeSingleObjectOnce();

    //    sleep(5);

    //    ARMARX_INFO_S << "=============================================================";
    //    ARMARX_INFO_S << "USECASE II: query a specific object for continuous observation";
    //    ARMARX_INFO_S << "=============================================================";
    //    LocalizeSingleObjectRepeated();

    //    sleep(5);

    ARMARX_INFO_S << "=============================================================";
    ARMARX_INFO_S << "USECASE III: query for an ontological class of objects";
    ARMARX_INFO_S << "=============================================================";
    WhatCanYouSeeNow();

    sleep(5);
}


void RequestClass::onDisconnectComponent()
{

}


void RequestClass::onExitComponent()
{

}

void RequestClass::LocalizeSingleObjectOnce()
{
    /////////////////////////////////////////////////////////////////////////////////////////////////
    // USECASE I: query a specific object and wait until localization finished
    /////////////////////////////////////////////////////////////////////////////////////////////////

    ARMARX_INFO_S << "Initiating localization of blue cup once ..." << std::endl;

    // 1. request the object class (corresponding to the classes in prior knowledge see PriorMemoryEditorGui)
    ChannelRefBasePtr classChannel1 = observer->requestObjectClassOnce("cupblue");

    if (!classChannel1)
    {
        ARMARX_ERROR_S << "Could not find class in prior knowledge" << std::endl;
        return;
    }


    // 2. install condition on the localizationFinished datafield
    Literal objectLocalizationFinished1(ChannelRefPtr::dynamicCast(classChannel1)->getDataFieldIdentifier("localizationFinished"), "equals", Literal::createParameterList(true));
    EventBasePtr e1 = new EventBase();
    ConditionIdentifier id1 = conditionHandler->installCondition(EventListenerInterfacePrx::uncheckedCast(getProxy()), objectLocalizationFinished1.getImpl(), e1, true);

    // usually we would wait for the event. Here we just sleep a bit, since we are not in a statechart
    sleep(5);

    // 3. fetch object instances for the requested object class
    ChannelRefBaseSequence channelRefs = observer->getObjectInstances(classChannel1);
    ChannelRefBaseSequence::iterator iter = channelRefs.begin();
    ARMARX_INFO_S << "Found the following instance:";

    while (iter != channelRefs.end())
    {
        ChannelRefPtr channelRef = ChannelRefPtr::dynamicCast(*iter);
        ARMARX_INFO_S << channelRef->getDataField("instanceName");
        iter++;
    }

    // 4. release object class (do not forget)
    observer->releaseObjectClass(classChannel1);

    conditionHandler->removeCondition(id1);
}

void RequestClass::LocalizeSingleObjectRepeated()
{
    /////////////////////////////////////////////////////////////////////////////////////////////////
    // USECASE II: query a specific object for continuous observation (e.g for visual servoing
    /////////////////////////////////////////////////////////////////////////////////////////////////

    ARMARX_INFO_S << "Initiating localization of green cup repeated ..." << std::endl;

    // 1. request the object class (corresponding to the classes in prior knowledge see PriorMemoryEditorGui)
    ChannelRefBasePtr classChannel1 = observer->requestObjectClassRepeated("cupblue", 500);

    if (!classChannel1)
    {
        ARMARX_ERROR_S << "Could not find class in prior knowledge" << std::endl;
        return;
    }

    // 2. install condition on the localizationFinished datafield
    Literal objectLocalizationFinished1(ChannelRefPtr::dynamicCast(classChannel1)->getDataFieldIdentifier("localizationFinished"), "equals", Literal::createParameterList(true));
    EventBasePtr e1 = new EventBase();
    ConditionIdentifier id1 = conditionHandler->installCondition(EventListenerInterfacePrx::uncheckedCast(getProxy()), objectLocalizationFinished1.getImpl(), e1, true);

    // usually we would wait for the event. Here we just sleep a bit, since we are not in a statechart
    sleep(5);

    // 3. fetch object instances for the requested object class
    ChannelRefBaseSequence channelRefs = observer->getObjectInstances(classChannel1);

    // now we need to choose a specific instance (there could be more then one in the scene)
    if (channelRefs.size() > 0)
    {
        ChannelRefBasePtr channel = *channelRefs.begin();

        // we would now use this channel as output parameter of the state and as input for the visual servoing
    }

    // 4. release object class (do not forget)
    observer->releaseObjectClass(classChannel1);

    conditionHandler->removeCondition(id1);
}

void RequestClass::WhatCanYouSeeNow()
{
    /////////////////////////////////////////////////////////////////////////////////////////////////
    // USECASE III: query for an ontological class of objects
    /////////////////////////////////////////////////////////////////////////////////////////////////

    ARMARX_INFO_S << "Initiating localization of all objects once ..." << std::endl;

    // 1. request the object class (corresponding to the classes in prior knowledge see PriorMemoryEditorGui)
    ChannelRefBasePtr classChannel1 = observer->requestObjectClassOnce("all");

    if (!classChannel1)
    {
        ARMARX_ERROR_S << "Could not find class in prior knowledge" << std::endl;
        return;
    }

    // 2. install condition on the localizationFinished datafield
    Literal objectLocalizationFinished1(ChannelRefPtr::dynamicCast(classChannel1)->getDataFieldIdentifier("localizationFinished"), "equals", Literal::createParameterList(true));
    EventBasePtr e1 = new EventBase();
    ConditionIdentifier id1 = conditionHandler->installCondition(EventListenerInterfacePrx::uncheckedCast(getProxy()), objectLocalizationFinished1.getImpl(), e1, true);

    // usually we would wait for the event. Here we just sleep a bit, since we are not in a statechart
    sleep(5);

    // 3. fetch object instances for the requested object class
    ChannelRefBaseSequence channelRefs = observer->getObjectInstances(classChannel1);
    ChannelRefBaseSequence::iterator iter = channelRefs.begin();
    ARMARX_INFO_S << "Found the following instance: (also includes instances not visible but localized earlier)";

    while (iter != channelRefs.end())
    {
        ChannelRefPtr channelRef = ChannelRefPtr::dynamicCast(*iter);
        ARMARX_INFO_S << channelRef->getDataField("instanceName");
        iter++;
    }

    // 4. release object class (do not forget)
    observer->releaseObjectClass(classChannel1);

    conditionHandler->removeCondition(id1);
}
