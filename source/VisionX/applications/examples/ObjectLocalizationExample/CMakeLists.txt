armarx_component_set_name("ObjectLocalizationExampleApp")
set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore MemoryXInterfaces ArmarXCoreObservers RobotAPICore)
set(EXE_SOURCES
    ObjectLocalizationExampleApp.cpp
    RequestClass.cpp
    main.cpp)
armarx_add_component_executable("${EXE_SOURCES}")

