armarx_component_set_name(TexturedObjectRecognition)
set(COMPONENT_LIBS VisionXObjectPerception
                   VisionXInterfaces
                   VisionXCore
                   VisionXTools
                   ArmarXCoreInterfaces
                   ArmarXCore
                   RobotAPICore
                   ArmarXCoreObservers
                   MemoryXInterfaces
                   MemoryXCore
                   MemoryXObjectRecognitionHelpers
                   MemoryXEarlyVisionHelpers)
set(EXE_SOURCE main.cpp)
armarx_add_component_executable("${EXE_SOURCE}")
