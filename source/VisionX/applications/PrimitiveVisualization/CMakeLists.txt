armarx_component_set_name("PrimitiveVisualizationApp")

set(COMPONENT_LIBS
    ArmarXCoreInterfaces
    ArmarXCore
    PrimitiveVisualization
)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
