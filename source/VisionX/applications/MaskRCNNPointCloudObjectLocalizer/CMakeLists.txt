armarx_component_set_name("MaskRCNNPointCloudObjectLocalizerApp")

find_package(PCL 1.7 QUIET)
find_package(Simox QUIET)

armarx_build_if(PCL_FOUND "PCL not available")
armarx_build_if(Simox_FOUND "Simox not available")

if(PCL_FOUND AND Simox_FOUND)
    include_directories(${Simox_INCLUDE_DIRS})
    include_directories(SYSTEM ${PCL_INCLUDE_DIRS})
    link_directories(${PCL_LIBRARY_DIRS})
    add_definitions(${PCL_DEFINITIONS})
endif()

set(COMPONENT_LIBS
    MaskRCNNPointCloudObjectLocalizer
    VisionXInterfaces
    VisionXTools
    VisionXCore
    ArmarXCoreInterfaces
    ArmarXCore
    ${Simox_LIBRARIES}
)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
