/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::playback
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <VisionX/libraries/playback/AbstractPlaybackStrategy.h>
#include <VisionX/libraries/playback/public_api.h>




/**
 * This package provides an API to read recordings frame-by-frame of any type and obtain their metadata.
 *
 * Current available formats are:
 *
 *  - Video formats (`.avi`, `.mp4`; provided by OpenCV2) via VideoPlaybackStrategy
 *  - Loose image collections (any image format) via ImageSequencePlaybackStrategy
 *  - Chunked image collections (ImageMonitor recording output) via ChunkedImageSequencePlaybackStrategy
 *
 * Required header:
 *
 * ```cpp
 * // Include the required header. NOTE: Other header files are not part of the public API and may be subject to change
 * #include <VisionX/libraries/playback.h>
 * ```
 *
 * Example (Play back an AVI recording):
 *
 * ```cpp
 * // Initialise replay
 * const std::filesystem::path path = "/home/anon/recording_2010-10-10.avi";
 * visionx::playback::Playback rep = visionx::playback::newPlayback(path);
 *
 * // Read frames
 * while (rep->hasNextFrame())
 * {
 *     cv::Mat frame;
 *     rep->getNextFrame(frame); // ::CByteImage or a raw unsigned char pointer are supported as well, albeit discouraged
 *     // ... process/display/manipulate frame ...
 * }
 *
 * // Stop playback
 * rep->stopPlayback();
 *
 * // {rep} is now uninitialised and should be destroyed
 * ```
 *
 * @package visionx::playback
 * @author Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date 2018
 */
namespace visionx::playback
{
    // pass
}

