#pragma once

#include <VisionX/libraries/VoxelGridCore/Visualizer.hpp>

#include "VoxelGrid.h"


namespace visionx::voxelgrid::LabelOccupancy
{

    /**
     * @brief Visualizer for label occupancy voxel grids.
     *
     * Draws occupied voxels as single colored boxes according to their label.
     */
    class Visualizer : public voxelgrid::Visualizer<Voxel>
    {

    public:

        Visualizer();
        Visualizer(const armarx::DebugDrawerTopic& drawer,
                   const std::string& layer = "LabelOccupancyVoxelGrid");

        virtual ~Visualizer() override;


        /// Get the alpha.
        float getAlpha() const;
        /// Set the alpha. Must be in [0, 1].
        void setAlpha(float value);


    protected:

        virtual bool isVisible(const VoxelVisuData& voxelVisu) const override;
        virtual void drawVoxel(const VoxelVisuData& voxelVisu) override;


        float alpha = 0.75;

    };

}
