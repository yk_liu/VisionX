/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MA_RainerKartmann::ArmarXObjects::PointCloudMerger
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <set>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <VisionX/interface/core/DataTypes.h>


namespace visionx
{

    /**
     * @brief Merges several labeled or unlabeled point clouds into one
     * labeled point cloud.
     *
     *
     */
    class MergedLabeledPointCloud
    {
    public:

        using PointT = pcl::PointXYZRGBA;
        using PointL = pcl::PointXYZRGBL;
        using Label = uint32_t;


    public:

        /// Constructor.
        MergedLabeledPointCloud();


        // Input.

        /**
         * @brief Set a (new) labeled or unlabeled input point cloud.
         *
         * Stores the given point cloud as latest point cloud with the given
         * name, which will be used when getting the result point cloud.
         * If `isLabeled` is passed as false, the point cloud will receive a
         * (new) constant label.
         *
         * @param name The point cloud's name.
         * @param inputCloud The input point cloud.
         * @param isLabeld Whether the point cloud has valid labels.
         */
        void setInputPointCloud(const std::string& name, pcl::PointCloud<PointL>::ConstPtr inputCloud,
                                bool isLabeled = true);
        /**
         * @brief Set a (new) input point cloud.
         * @param name The point cloud's name
         * @param inputCloud The input point cloud.
         * @param originalType The providers point type (used to check whether the
         *      cloud is labeled or not).
         */
        void setInputPointCloud(const std::string& name, pcl::PointCloud<PointL>::ConstPtr inputCloud,
                                visionx::PointContentType originalType);
        /**
         * @brief Set a (new) unlabeled input point cloud.
         * The point cloud will receive a (new) constant label.
         *
         * @param name The point cloud name.
         * @param inputCloud The input point cloud.
         */
        void setInputPointCloud(const std::string& name, pcl::PointCloud<PointT>::ConstPtr inputCloud);


        // Result.

        /**
         * @brief Get the result point cloud.
         * It is rebuild if necessary.
         *
         * @return The result point cloud.
         */
        pcl::PointCloud<PointL>::Ptr getResultPointCloud();


    private:

        /// Get the labels present in `inputLabeledClouds`.
        std::set<Label> getUsedLabels() const;
        /// Label unlabeled clouds and move them to `inputLabeledClouds`.
        void labelUnlabeledClouds(const std::set<Label>& usedLabels);

        /// Rebuild `resultCloud`. Clears it and appends stored point clouds from all providers.
        void mergeLabeledClouds();


    private:

        /// Latest received unlabeled clouds.
        std::map<std::string, pcl::PointCloud<PointT>::ConstPtr> inputUnlabeledClouds;
        /// Latest received labeled clouds + unlabeled clouds with new labels.
        std::map<std::string, pcl::PointCloud<PointL>::ConstPtr> inputLabeledClouds;

        /// Whether input has changed and resultCloud must be rebuilt.
        bool changed = false;
        /// Result point cloud. Cached while no new input clouds arrive.
        pcl::PointCloud<PointL>::Ptr resultCloud { new pcl::PointCloud<PointL>() };

    };
}
