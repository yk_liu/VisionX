armarx_set_target("VisionXArViz")

set(SOURCES
    HumanPoseBody25.cpp
)

set(HEADERS
    HumanPoseBody25.h
)

set(COMPONENT_LIBS
    ArViz
    VisionXCore
)

armarx_add_library(VisionXArViz "${SOURCES}" "${HEADERS}" "${COMPONENT_LIBS}")
