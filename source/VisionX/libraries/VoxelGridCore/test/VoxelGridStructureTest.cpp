/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RelationsForManipulation::ArmarXObjects::SupportVoxelGrid
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::ArmarXLibraries::VoxelGridCore

#define ARMARX_BOOST_TEST

#include <VisionX/Test.h>
#include "../VoxelGridStructure.h"

#include <iostream>


using namespace visionx::voxelgrid;


static const Eigen::IOFormat iofVec = {3, 0, " ", " ", "", "", "[", "]"};
static const Eigen::IOFormat iofMat = {3, 0, " ", "\n", "[ ", " ]", "", ""};

#define BOOST_CHECK_EQUAL_VECTOR(lhs, rhs) \
    BOOST_CHECK_MESSAGE((lhs) == (rhs), "("#lhs ") == (" #rhs ") failed (" \
                        << lhs.format(iofVec) << " != " << rhs.format(iofVec) << ")")


struct Fixture
{
    Fixture()
    {
        BOOST_TEST_MESSAGE("Grid: " << structure);
    }

    VoxelGridStructure structure = {16, 1.f, {1, 2, 3}};
};



BOOST_FIXTURE_TEST_SUITE(VoxelGridStructureTestSuite, Fixture)


BOOST_AUTO_TEST_CASE(test_getCenter_zero_origin)
{
    structure.setVoxelSize(1);
    structure.setGridSize({ 2, 3, 4 });

    structure.setOrigin({0, 0, 0});

    /* Expected centers:
     *     2              3                4
     * +---+---+    +---+---+---+  +---+---+---+---+
     * |   c o |    |   |c=o|   |  |   |   c o |   |
     * +---+---+    +---+---+---+  +---+---+---+---+
     *     | |            |                | |
     *   -.5 0            0              -.5 0
     */

    Eigen::Vector3f center = structure.getCenter();
    BOOST_CHECK_EQUAL_VECTOR(center, Eigen::Vector3f(-0.5, 0, -0.5));
}

BOOST_AUTO_TEST_CASE(test_getCenter_nonzero_origin)
{
    structure.setVoxelSize(1);
    structure.setGridSize({ 2, 3, 4 });

    Eigen::Vector3f origin(-2, 3, 5);
    structure.setOrigin(origin);

    // see test_center_zero_origin
    // center must be shifted by origin

    Eigen::Vector3f center = structure.getCenter();
    BOOST_CHECK_EQUAL_VECTOR(center, (Eigen::Vector3f(-0.5, 0, -0.5) + origin).eval());
}


BOOST_AUTO_TEST_CASE(test_index_conversion_simple)
{
    for (std::size_t i = 0; i < structure.getNumVoxels(); ++i)
    {
        Eigen::Vector3i indices = structure.getGridIndex(i);
        Eigen::Vector3f center = structure.getVoxelCenter(i);

        BOOST_CHECK_EQUAL_VECTOR(structure.getGridIndex(i), indices);
        BOOST_CHECK_EQUAL_VECTOR(structure.getGridIndex(center), indices);
    }
}


BOOST_AUTO_TEST_CASE(test_index_conversion_shifted_centers)
{
    for (std::size_t i = 0; i < structure.getNumVoxels(); ++i)
    {
        Eigen::Vector3i indices = structure.getGridIndex(i);
        Eigen::Vector3f center = structure.getVoxelCenter(indices);

        BOOST_CHECK_EQUAL_VECTOR(structure.getGridIndex(center), indices);

        // shift in both directions
        for (int dir :
             {
                 - 1, 1
                 })
        {
            Eigen::Vector3f shifted = center + dir * 0.45f * structure.getVoxelSize();
            BOOST_CHECK_EQUAL_VECTOR(structure.getGridIndex(shifted), indices);
        }
    }
}


BOOST_AUTO_TEST_CASE(test_get_voxel_indices_corners)
{
    structure.setOrigin({0, 0, 0});

    Eigen::Matrix32f bb = structure.getLocalBoundingBox();
    Eigen::Matrix<int, 3, 2> bbi;
    bbi.col(0) << structure.getGridIndexMin();
    bbi.col(1) << structure.getGridIndexMax();

    BOOST_TEST_MESSAGE("Bounding box: \n" << bb.format(iofMat));
    BOOST_TEST_MESSAGE("Bounding indices: \n" << bbi.format(iofMat));

    for (int x :
         {
             0, 1
         }) for (int y :
                     {
                         0, 1
                     }) for (int z :
                                 {
                                     0, 1
                                 })
            {
                Eigen::Vector3i trueIndices;
                trueIndices << bbi.col(x).x(), bbi.col(y).y(), bbi.col(z).z();

                Eigen::Vector3f corner;
                corner << bb.col(x).x(), bb.col(y).y(), bb.col(z).z();


                // move slightly towards origin
                Eigen::Vector3f corner2Origin = structure.getOrigin() - corner;
                corner += 0.001f * corner2Origin;

                Eigen::Vector3i gotIndices = structure.getGridIndex(corner);

                BOOST_TEST_MESSAGE("Corner:         \t" << corner.format(iofVec));
                BOOST_TEST_MESSAGE("Shifted corner: \t" << corner.format(iofVec));
                BOOST_TEST_MESSAGE("Got indices:    \t" << gotIndices.format(iofVec));
                BOOST_TEST_MESSAGE("True indices:   \t" << trueIndices.format(iofVec));

                BOOST_CHECK(structure.isInside(gotIndices));
                BOOST_CHECK_EQUAL_VECTOR(gotIndices, trueIndices);
            }
}


BOOST_AUTO_TEST_SUITE_END()
