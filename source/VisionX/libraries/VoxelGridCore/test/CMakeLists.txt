
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore ${LIB_NAME})
 
armarx_add_test(VoxelGridTest           VoxelGridTest.cpp           "${LIBS}")
armarx_add_test(VoxelGridStructureTest  VoxelGridStructureTest.cpp  "${LIBS}")
armarx_add_test(VoxelLineTest           VoxelLineTest.cpp           "${LIBS}")
armarx_add_test(VoxelGridIOTest         VoxelGridIOTest.cpp         "${LIBS}")
armarx_add_test(VoxelGridJsonIOTest     VoxelGridJsonIOTest.cpp     "${LIBS}")
