#include "JsonIO.h"


namespace visionx::voxelgrid::io
{


    void JsonIO::writeJson(
        const std::string& filename, const nlohmann::json& j,
        int indent, char indentChar)
    {
        std::ofstream ofs(filename);
        writeJson(ofs, j, indent, indentChar);
    }

    void JsonIO::writeJson(
        std::ostream& os, const nlohmann::json& j,
        int indent, char indentChar)
    {
        os << j.dump(indent, indentChar);
    }

    nlohmann::json JsonIO::readJson(const std::string& filename)
    {
        std::ifstream ifs(filename);
        return readJson(ifs);
    }

    nlohmann::json JsonIO::readJson(std::istream& is)
    {
        nlohmann::json j;
        is >> j;
        return j;
    }



}
