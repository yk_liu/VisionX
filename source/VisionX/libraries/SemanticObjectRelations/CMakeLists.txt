armarx_set_target("VisionXSemanticObjectRelations")

find_package(SemanticObjectRelations QUIET)
armarx_build_if(SemanticObjectRelations_FOUND "SemanticObjectRelations not available")

set(SOURCES
    json_serialization.cpp
    JsonSimoxShapeSerializer.cpp
    memory_serialization.cpp
    shapes_from_aabbs.cpp
    SimoxObjectShape.cpp

    ice_serialization/graph.cpp
    ice_serialization/shape.cpp

    hooks.cpp
    hooks/ArmarXLog.cpp
    hooks/ArmarXVisualizer.cpp
)

set(HEADERS
    json_serialization.h
    JsonSimoxShapeSerializer.h
    memory_serialization.h
    shapes_from_aabbs.h
    SimoxObjectShape.h

    ice_serialization.h
    ice_serialization/graph.h
    ice_serialization/shape.h

    hooks.h
    hooks/ArmarXLog.h
    hooks/ArmarXVisualizer.h
)

set(COMPONENT_LIBS
    SemanticObjectRelations

    ArmarXCore
    MemoryXMemoryTypes MemoryXVirtualRobotHelpers
    VisionXInterfaces VisionXPointCloudTools
)

armarx_add_library(VisionXSemanticObjectRelations "${SOURCES}" "${HEADERS}" "${COMPONENT_LIBS}")

if(SemanticObjectRelations_FOUND)
    target_include_directories(VisionXSemanticObjectRelations PUBLIC ${SemanticObjectRelations_INCLUDE_DIRS})
endif()
