#pragma once

#include <SemanticObjectRelations/Shapes/shape_containers.h>

#include <VisionX/interface/libraries/SemanticObjectRelations/Shape.h>


namespace armarx::semantic
{

    data::Shape toIce(const semrel::Shape& shape);
    semrel::ShapePtr fromIce(const data::Shape& shape);

    data::ShapeList toIce(const semrel::ShapeList& shapes);
    data::ShapeList toIce(const semrel::ShapeMap& shapes);
    semrel::ShapeList fromIce(const data::ShapeList& shapes);

}
