#include "hooks.h"


namespace armarx
{

    void semantic::setArmarXHooksAsImplementation(
        const DebugDrawerInterfacePrx& debugDrawer,
        const std::string& logTag)
    {
        ArmarXLog::setAsImplementation(logTag);
        ArmarXVisualizer::setAsImplementation(debugDrawer);
    }

}
