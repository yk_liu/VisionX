#include "memory_serialization.h"

#include <MemoryX/core/entity/EntityRef.h>
#include <MemoryX/libraries/memorytypes/entity/Relation.h>


namespace armarx
{
    memoryx::RelationList semantic::toMemory(const semrel::AttributedGraph& graph)
    {
        const std::string type = graph.getTypeName();

        memoryx::RelationList relations;
        for (auto edge : graph.edges())
        {
            memoryx::RelationPtr relation(new memoryx::Relation());
            relation->setName(type);
            // sign == true: the relation holds
            // sign == false: the relation does not hold
            relation->setSign(true);

            auto makeRef = [&](const nlohmann::json & vertex)
            {
                memoryx::EntityRefPtr ref(new memoryx::EntityRef());
                ref->entityId = vertex.at("object").at("entityId").get<std::string>();
                return ref;
            };
            auto& source = edge.source().attrib().json;
            auto& target = edge.target().attrib().json;
            memoryx::EntityRefPtr sourceRef = makeRef(source);
            memoryx::EntityRefPtr targetRef = makeRef(target);

            // Order matters! The relation is directed: source -> target
            relation->setEntities({sourceRef, targetRef});

            relation->setAttributes(edge.attrib().json.dump());
            relation->setSourceAttributes(source.dump());
            relation->setTargetAttributes(target.dump());

            relations.push_back(relation);
        }

        return relations;
    }

    semrel::AttributedGraph semantic::fromMemory(const memoryx::RelationList& relations)
    {
        // Find all unique entities in the graph and store their attributes.
        std::string type;
        std::map<std::string, nlohmann::json> id2attr;
        std::map<std::pair<std::string, std::string>, nlohmann::json> edge2attr;

        for (const memoryx::RelationBasePtr& relationBase : relations)
        {
            type = relationBase->getName();
            auto relation = memoryx::RelationPtr::dynamicCast(relationBase);
            // sign == true: The relation holds
            if (relation && relation->getSign() == true)
            {
                memoryx::EntityRefList entities = relation->getEntities();
                if (entities.size() == 2)
                {
                    auto& sourceId = entities[0]->entityId;
                    auto& targetId = entities[1]->entityId;
                    auto sourceAttrs = nlohmann::json::parse(relation->getSourceAttributes());
                    auto targetAttrs = nlohmann::json::parse(relation->getTargetAttributes());
                    id2attr.emplace(sourceId, sourceAttrs);
                    id2attr.emplace(targetId, targetAttrs);

                    auto edgeAttrs = nlohmann::json::parse(relation->getAttributes());
                    edge2attr.emplace(std::make_pair(sourceId, targetId), edgeAttrs);
                }
            }
        }

        // Create an attributed graph from the two maps id2attr and edge2attr.
        semrel::AttributedGraph result;
        result.attrib().json =
        {
            {"type", type}
        };
        std::map<std::string, semrel::AttributedGraph::VertexDescriptor> id2desc;
        {
            for (auto& [id, attr] : id2attr)
            {
                semrel::AttributedGraph::Vertex vertex = result.addVertex(semrel::ShapeID(-1));
                vertex.attrib().json = std::move(attr);
                id2desc.emplace(id, vertex.descriptor());
            }

            for (auto& [pair, attr] : edge2attr)
            {
                auto& sourceId = pair.first;
                auto& targetId = pair.second;

                auto sourceDesc = id2desc.at(sourceId);
                auto targetDesc = id2desc.at(targetId);

                semrel::AttributedGraph::Edge edge = result.addEdge(sourceDesc, targetDesc);
                edge.attrib().json = attr;
            }
        }

        return result;
    }
}
