#pragma once

#include "hooks/ArmarXLog.h"
#include "hooks/ArmarXVisualizer.h"


namespace armarx::semantic
{

    void setArmarXHooksAsImplementation(const DebugDrawerInterfacePrx& debugDrawer,
                                        const std::string& logTag = "SemanticObjectRelations");

}
