/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <string>

// OpenCV 2
#include <opencv2/opencv.hpp>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>

// VisionX
#include <VisionX/libraries/record/AbstractSequencedRecordingStrategy.h>


namespace visionx::record::strats
{
    /**
     * Concrete strategy for a PNG image recording
     */
    class PNGRecordingStrategy;
}



class visionx::record::strats::PNGRecordingStrategy :
    public visionx::record::AbstractSequencedRecordingStrategy
{

private:

    const bool snapshotMode;

public:

    PNGRecordingStrategy(bool snapshotMode = false);
    PNGRecordingStrategy(const std::filesystem::path& filePath, unsigned int fps);
    virtual ~PNGRecordingStrategy() override;

    virtual bool isRecording() const override;
    virtual void startRecording(const std::filesystem::path& filePath, unsigned int fps) override;
    virtual void stopRecording() override;

    virtual void recordSnapshot(const cv::Mat& image, const std::filesystem::path& path);
    virtual void recordSnapshot(const CByteImage& image, const std::filesystem::path& path);

protected:

    virtual void recordFrame(const cv::Mat& frame, const std::filesystem::path& fullPath) override;

    /**
     * @brief Writes a file to the metadata file if writing is enabled only if not in snapshotMode
     * @param varName Name of the variable to be written
     * @param varType Type of the variable to be written
     * @param varValue Value of the variable to be written
     */
    virtual void writeMetadataLine(const std::string& varName, const std::string& varType, const std::string& varValue) override;

};
