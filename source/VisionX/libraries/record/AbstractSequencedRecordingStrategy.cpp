/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// Amount of frames per chunk
#define CHUNK_SIZE 100


#include <VisionX/libraries/record/AbstractSequencedRecordingStrategy.h>


// Boost
#include <filesystem>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

// VisionX
#include <VisionX/libraries/record/helper.h>


visionx::record::AbstractSequencedRecordingStrategy::AbstractSequencedRecordingStrategy() :
    visionx::record::AbstractRecordingStrategy()
{
    // pass
}


visionx::record::AbstractSequencedRecordingStrategy::AbstractSequencedRecordingStrategy(const std::filesystem::path& filePath, unsigned int fps) :
    visionx::record::AbstractRecordingStrategy(filePath, fps)
{
    this->startRecording(filePath, fps);
}


visionx::record::AbstractSequencedRecordingStrategy::~AbstractSequencedRecordingStrategy()
{
    // pass
}


void
visionx::record::AbstractSequencedRecordingStrategy::startRecording(const std::filesystem::path&, unsigned int)
{
    std::filesystem::path pathStem = this->getPath() / this->getStem();

    ARMARX_CHECK_EXPRESSION_W_HINT(!std::filesystem::exists(pathStem), "Folder already exists");

    std::filesystem::create_directory(pathStem);

    this->writeMetadataLine("fps", "unsigned int", std::to_string(this->getFps()));
    this->writeMetadataLine("framesPerChunk", "unsigned int", std::to_string(CHUNK_SIZE));
    this->writeMetadataLine("extension", "string", this->getDotExtension().string());
}


void
visionx::record::AbstractSequencedRecordingStrategy::recordFrame(const CByteImage& frame)
{
    if (this->sequenceNumber == 0)
    {
        this->writeMetadataLine("frameHeight", "unsigned int", std::to_string(static_cast<unsigned int>(frame.height)));
        this->writeMetadataLine("frameWidth", "unsigned int", std::to_string(static_cast<unsigned int>(frame.width)));
    }

    this->recordFrame(frame, this->getNextFullPath());
}


void
visionx::record::AbstractSequencedRecordingStrategy::recordFrame(const cv::Mat& frame)
{
    if (this->sequenceNumber == 0)
    {
        this->writeMetadataLine("frameHeight", "unsigned int", std::to_string(static_cast<unsigned int>(frame.size().height)));
        this->writeMetadataLine("frameWidth", "unsigned int", std::to_string(static_cast<unsigned int>(frame.size().width)));
    }

    this->recordFrame(frame, this->getNextFullPath());
}


void
visionx::record::AbstractSequencedRecordingStrategy::stopRecording()
{
    this->writeMetadataLine("frameCount", "unsigned int", std::to_string(this->sequenceNumber));
    if (this->metadataFile.is_open())
    {
        this->metadataFile.close();
    }
}


void
visionx::record::AbstractSequencedRecordingStrategy::recordFrame(const CByteImage& frame, const std::filesystem::path& fullPath)
{
    // Default implementations of recordFrame just convert the parameter and call the other recordFrame method.
    // This way only one method needs to be overridden in the interface-implementing classes

    // Call CByteImage variant of recordFrame(...)
    cv::Mat cv_frame;
    visionx::record::convert(frame, cv_frame);
    this->recordFrame(cv_frame, fullPath);
}


void
visionx::record::AbstractSequencedRecordingStrategy::recordFrame(const cv::Mat& frame, const std::filesystem::path& fullPath)
{
    // Default implementations of recordFrame just convert the parameter and call the other recordFrame method.
    // This way only one method needs to be overridden in the interface-implementing classes

    // Call cv::Mat variant of recordFrame(...)
    CByteImage ivt_frame;
    visionx::record::convert(frame, ivt_frame);
    this->recordFrame(ivt_frame, fullPath);
}


void
visionx::record::AbstractSequencedRecordingStrategy::writeMetadataLine(const std::string& varName, const std::string& varType, const std::string& varValue)
{
    if (!this->metadataFile.is_open())
    {
        std::filesystem::path path = this->getPath() / this->getStem();
        this->metadataFile.open((std::filesystem::canonical(path) / "metadata.csv").string());
        this->writeMetadataLine("name", "type", "value");
    }

    // If the given variable was already written, do nothing
    if (this->writtenMetadataVariables.insert(varName).second == false)
    {
        return;
    }

    this->metadataFile << varName << "," << varType << "," << varValue << "\n";
    this->metadataFile.flush();
}


std::filesystem::path
visionx::record::AbstractSequencedRecordingStrategy::getNextFullPath()
{
    const std::filesystem::path frameFilename = std::filesystem::path("frame_" + std::to_string(this->sequenceNumber)).replace_extension(this->getDotExtension());

    if (this->sequenceNumber % CHUNK_SIZE == 0)
    {
        unsigned int chunk_number = this->sequenceNumber / CHUNK_SIZE;
        this->framesChunk = "chunk_" + std::to_string(chunk_number);
        std::filesystem::create_directory(std::filesystem::canonical(this->getPath() / this->getStem()) / this->framesChunk);
    }

    ++this->sequenceNumber;

    return std::filesystem::canonical(this->getPath() / this->getStem() / this->framesChunk) / frameFilename;
}
