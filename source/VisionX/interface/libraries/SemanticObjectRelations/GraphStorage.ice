#pragma once

#include <VisionX/interface/libraries/SemanticObjectRelations/Graph.ice>


module armarx
{
    module semantic
    {
        interface GraphStorageTopic
        {
            void reportGraph(string id, data::Graph graph);
        };

        interface GraphStorageInterface
        {
            data::GraphMap getAll();

            void remove(string id);

            void removeAll();
        };

        interface GraphStorageTopicAndInterface extends GraphStorageTopic, GraphStorageInterface
        {

        };
    };
};
