/**
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     Jan Issac <jan dot issac at gmx dot de>
 * @copyright  2010 Humanoids Group, HIS, KIT
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/interface/core/DataTypes.ice>
#include <VisionX/interface/core/ImageProviderInterface.ice>
#include <VisionX/interface/core/ImageProcessorInterface.ice>

module visionx
{
    struct CameraParameters
    {
        int width;
        int height;
        types::Vec principalPoint;
        types::Vec focalLength;
        types::Vec distortion;
        types::Mat rotation;
        types::Vec translation;        
    };
    
    struct MonocularCalibration
    {
        CameraParameters cameraParam;
    };
    
    struct StereoCalibration
    {
        MonocularCalibration calibrationLeft;
        MonocularCalibration calibrationRight;
        types::Mat rectificationHomographyLeft;
        types::Mat rectificationHomographyRight;
    };

    interface MonocularCalibrationCapturingProviderInterface extends CapturingImageProviderInterface
    {
        MonocularCalibration getCalibration();
    };


    interface ReferenceFrameInterface
    {
        string getReferenceFrame();
    };

    interface StereoCalibrationInterface extends ReferenceFrameInterface
    {
        StereoCalibration getStereoCalibration();
        bool getImagesAreUndistorted();
    };

    /**
      * Interface for implementation of an stereo image provider
      */
    interface StereoCalibrationProviderInterface extends CompressedImageProviderInterface, StereoCalibrationInterface
    {
    };

    /**
      * Interface for implementation of an stereo image processor
      */
    interface StereoCalibrationProcessorInterface extends ImageProcessorInterface, StereoCalibrationInterface
    {
    };

    /**
      * Interface for implementation of an stereo capturing image provider
      */
    interface StereoCalibrationCaptureProviderInterface extends CapturingImageProviderInterface, StereoCalibrationProviderInterface
    {
    };


    interface ImageFileSequenceProviderInterface extends StereoCalibrationCaptureProviderInterface
    {
        // Set the file paths. If the images are not stereo, leave the right path empty
        void setImageFilePath(string imageFilePathLeft, string imageFilePathRight);
        void loadNextImage();
    };
        
};
 
