/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/util/CPPUtility/trace.h>

#include "ImageProvider.h"
#include <opencv2/opencv.hpp>
#include <string>
#include <VisionX/libraries/record/helper.h>
#include <VisionX/tools/TypeMapping.h>
#include <VisionX/tools/ImageUtil.cpp>
using namespace armarx;
namespace visionx
{

    // ================================================================== //
    // == ImageProvider ice interface =================================== //
    // ================================================================== //
    armarx::Blob ImageProvider::getImages(const Ice::Current& c)
    {
        if (numberImages == 0)
        {
            return armarx::Blob();
        }

        return sharedMemoryProvider->getData();
    }


    armarx::Blob ImageProvider::getImagesAndMetaInfo(armarx::MetaInfoSizeBasePtr& info, const Ice::Current&)
    {
        if (numberImages == 0)
        {
            return armarx::Blob();
        }
        return sharedMemoryProvider->getData(info);
    }


    ImageFormatInfo ImageProvider::getImageFormat(const Ice::Current& c)
    {
        return imageFormat;
    }

    int ImageProvider::getNumberImages(const Ice::Current& c)
    {
        return numberImages;
    }

    // ================================================================== //
    // == Component implementation =============================== //
    // ================================================================== //
    void ImageProvider::onInitComponent()
    {
        ARMARX_TRACE;
        // init members
        exiting = false;

        // default image format (640x480, bayerpattern).
        // call from within init to change!
        setImageFormat(ImageDimension(640, 480), eBayerPattern, eBayerPatternRg);
        setNumberImages(0);

        // call setup of image provider implementation to setup image size
        onInitImageProvider();
    }


    void ImageProvider::onConnectComponent()
    {
        ARMARX_INFO << "onConnectComponent " << getName();
        ARMARX_TRACE;
        // init shared memory
        int imageSize = getImageFormat().dimension.width *  getImageFormat().dimension.height * getImageFormat().bytesPerPixel;

        if (numberImages != 0)
        {
            MetaInfoSizeBasePtr info(new MetaInfoSizeBase(getNumberImages() * imageSize, getNumberImages() * imageSize, 0));
            if (sharedMemoryProvider)
            {
                sharedMemoryProvider->stop();
            }
            sharedMemoryProvider = new IceSharedMemoryProvider<unsigned char>(this, info, "ImageProvider");

            // reference to shared memory
            imageBuffers = (void**) new unsigned char* [getNumberImages()];

            for (int i = 0 ; i < getNumberImages() ; i++)
            {
                imageBuffers[i] = sharedMemoryProvider->getBuffer() + i * imageSize;
            }

            // offer topic for image events
            offeringTopic(getName() + ".ImageListener");

            // retrieve storm topic proxy
            imageProcessorProxy = getTopic<ImageProcessorInterfacePrx>(getName() + ".ImageListener");

            // start icesharedmemory provider
            sharedMemoryProvider->start();
        }

        onConnectImageProvider();
    }

    void ImageProvider::onDisconnectComponent()
    {
        ARMARX_TRACE;
        onDisconnectImageProvider();
        if (sharedMemoryProvider)
        {
            sharedMemoryProvider->stop();
            sharedMemoryProvider = nullptr;
        }
    }

    void ImageProvider::onExitComponent()
    {
        ARMARX_TRACE;
        exiting = true;

        onExitImageProvider();

        if (numberImages != 0)
        {
            delete [] imageBuffers;
        }
    }

    void ImageProvider::updateTimestamp(Ice::Long timestamp, bool threadSafe)
    {
        ARMARX_TRACE;
        auto sharedMemoryProvider = this->sharedMemoryProvider; // preserve from deleting
        if (!sharedMemoryProvider)
        {
            ARMARX_INFO << "Shared memory provider is null!"
                        << " Did you forget to set call setNumberImages and setImageFormat in onInitImageProvider?";
            return;
        }
        MetaInfoSizeBasePtr info = this->sharedMemoryProvider->getMetaInfo(threadSafe);
        if (info)
        {
            info->timeProvided = timestamp;
        }
        else
        {
            info = new MetaInfoSizeBase(imageFormat.dimension.width * imageFormat.dimension.height * imageFormat.bytesPerPixel,
                                        imageFormat.dimension.width * imageFormat.dimension.height * imageFormat.bytesPerPixel,
                                        timestamp);
        }

        this->sharedMemoryProvider->setMetaInfo(info, threadSafe);
    }

    void ImageProvider::updateTimestamp(IceUtil::Time timestamp, bool threadSafe)
    {
        ARMARX_TRACE;
        updateTimestamp(timestamp.toMicroSeconds(), threadSafe);
    }

    Blob ImageProvider::getCompressedImagesAndMetaInfo(CompressionType compressionType, Ice::Int compressionQuality, MetaInfoSizeBasePtr& info, const Ice::Current&)
    {
        int type;
        switch (imageFormat.type)
        {
            case eRgb:
                type = CV_8UC3;
                break;
            case eGrayScale:
                type = CV_8UC1;
                break;
            default:
                throw LocalException() << "unsupported image type " << (int)(imageFormat.type);
        }

        if (!sharedMemoryProvider)
            return {};
        auto lock = sharedMemoryProvider->getScopedReadLock();
        if (!lock) // already shutdown
        {
            return {};
        }
        if (numberImages == 0)
        {
            return armarx::Blob();
        }
        info = new armarx::MetaInfoSizeBase(*sharedMemoryProvider->getMetaInfo());

        auto imageTimestamp = IceUtil::Time::microSeconds(info->timeProvided);
        auto key = std::make_pair(compressionType, compressionQuality);

        ScopedLock lock2(compressionDataMapMutex);
        if (compressionDataMap.count(key) && compressionDataMap.at(key).imageTimestamp == imageTimestamp)
        {
            ARMARX_VERBOSE << deactivateSpam(1) << "using already compressed image";
            return compressionDataMap.at(key).compressedImage;
        }
        cv::Mat mat(imageFormat.dimension.height * numberImages, imageFormat.dimension.width, type, imageBuffers[0]);


        Blob encodedImg;

        std::vector<int> compression_params;
        std::string extension;
        switch (compressionType)
        {
            case ePNG:
                extension = ".png";
                compression_params =  {CV_IMWRITE_PNG_COMPRESSION, compressionQuality,
                                       CV_IMWRITE_PNG_STRATEGY, CV_IMWRITE_PNG_STRATEGY_RLE
                                  };
                break;
            case eJPEG:
                extension = ".jpg";
                compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
                compression_params.push_back(compressionQuality);
                break;
            default:
                throw LocalException() << "unsupported image type " << (int)(imageFormat.type);
        }
        cv::imencode("*" + extension, mat, encodedImg, compression_params);

        compressionDataMap[key] = {encodedImg, imageTimestamp};

        //    cv::imwrite("/tmp/compressed" + extension, mat, compression_params);
        ARMARX_DEBUG << deactivateSpam(1) << "size before compression: " << imageFormat.dimension.height* numberImages* imageFormat.dimension.width* imageFormat.bytesPerPixel << " size after: " << encodedImg.size();
        return encodedImg;
    }


    // ================================================================== //
    // == Utility methods for ImageProviders ============================ //
    // ================================================================== //
    void ImageProvider::setImageFormat(ImageDimension imageDimension,
                                       ImageType imageType,
                                       BayerPatternType bayerPatternType)
    {
        ARMARX_TRACE;
        imageFormat.dimension  = imageDimension;
        imageFormat.type = imageType;
        imageFormat.bpType = bayerPatternType;

        switch (imageType)
        {
            case eGrayScale:
            case eBayerPattern:
                imageFormat.bytesPerPixel = 1;
                break;

            case eRgb:
                imageFormat.bytesPerPixel = 3;
                break;

            case eFloat1Channel:
                imageFormat.bytesPerPixel = 4;
                break;

            case eFloat3Channels:
                imageFormat.bytesPerPixel = 12;
                break;

            case ePointsScan:
                imageFormat.bytesPerPixel = 12;
                break;

            case eColoredPointsScan:
                imageFormat.bytesPerPixel = 16;
                break;
        }
    }

    void ImageProvider::setNumberImages(int numberImages)
    {
        ARMARX_TRACE;
        this->numberImages = numberImages;
    }

    void ImageProvider::provideImages(void** inputBuffers, const IceUtil::Time& imageTimestamp)
    {
        ARMARX_TRACE;
        if (numberImages == 0)
        {
            ARMARX_INFO << "Number of images is 0 - thus none can be provided";
            return;
        }

        int imageSize = imageFormat.dimension.width * imageFormat.dimension.height * imageFormat.bytesPerPixel;

        // copy
        {
            // lock memory access
            armarx::SharedMemoryScopedWriteLockPtr lock = getScopedWriteLock();
            if (!lock) // already shutdown
            {
                return;
            }
            if (imageTimestamp > IceUtil::Time())
            {
                updateTimestamp(imageTimestamp, false);
            }
            for (int i = 0 ; i < numberImages ; i++)
            {
                memcpy(imageBuffers[i], inputBuffers[i], imageSize);
            }
        }

        // notify processors
        if (imageProcessorProxy)
        {
            ARMARX_DEBUG << "Notifying ImageProcessorProxy";
            imageProcessorProxy->reportImageAvailable(getName());
        }
        else
        {
            ARMARX_ERROR << deactivateSpam(4) << "imageProcessorProxy is NULL - could not report Image available";
        }
    }

    void ImageProvider::provideImages(CByteImage** images, const IceUtil::Time& imageTimestamp)
    {
        ARMARX_TRACE;
        if (numberImages == 0)
        {
            ARMARX_INFO << "Number of images is 0 - thus none can be provided";
            return;
        }

        //ISO C++ forbids variable length array [-Werror=vla] => use a vector (the array will be on the heap anyways)
        std::vector<void*> imageBuffers(numberImages);

        for (int i = 0 ; i < numberImages ; i++)
        {
            //ARMARX_VERBOSE << i;
            imageBuffers[i] = images[i]->pixels;
        }

        provideImages(imageBuffers.data(), imageTimestamp);
    }

    void ImageProvider::provideImages(const std::vector<CByteImageUPtr >& images, const IceUtil::Time& imageTimestamp)
    {
        ARMARX_TRACE;
        if (numberImages == 0)
        {
            ARMARX_INFO << "Number of images is 0 - thus none can be provided";
            return;
        }

        //ISO C++ forbids variable length array [-Werror=vla] => use a vector (the array will be on the heap anyways)
        std::vector<void*> imageBuffers(numberImages);

        for (int i = 0 ; i < numberImages ; i++)
        {
            //ARMARX_VERBOSE << i;
            imageBuffers[i] = images[i]->pixels;
        }

        provideImages(imageBuffers.data(), imageTimestamp);
    }

    void ImageProvider::provideImages(CFloatImage** images, const IceUtil::Time& imageTimestamp)
    {
        ARMARX_TRACE;
        if (numberImages == 0)
        {
            ARMARX_INFO << "Number of images is 0 - thus none can be provided";
            return;
        }

        //ISO C++ forbids variable length array [-Werror=vla] => use a vector (the array will be on the heap anyways)
        std::vector<void*> imageBuffers(numberImages);

        for (int i = 0 ; i < numberImages ; i++)
        {
            imageBuffers[i] = images[i]->pixels;
        }

        provideImages(imageBuffers.data(), imageTimestamp);
    }
}


