/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ParticleFilterFrameworkParallelized.h"

#include <ArmarXCore/core/logging/Logging.h>

#include <omp.h>

#include <stdio.h>

namespace visionx
{
    CParticleFilterFrameworkParallelized::CParticleFilterFrameworkParallelized(int nParticles, int nDimension, int nNumParallelThreads) : CParticleFilterFramework(nParticles, nDimension)
    {
        m_nNumParallelThreads = nNumParallelThreads;
    }


    double CParticleFilterFrameworkParallelized::ParticleFilter(double* pResultMeanConfiguration, double dSigmaFactor, int nNumParticlesToUse)
    {
        // if desired, use only a smaller number of particles
        const int nRealNumParticles = m_nParticles;
        if (nNumParticlesToUse > 0 && nNumParticlesToUse < m_nParticles)
        {
            m_nParticles = nNumParticlesToUse;
        }

        // push previous state through process model
        // use dynamic model and add noise
        PredictNewBases(dSigmaFactor);

        // apply bayesian measurement weighting
        #pragma omp parallel for num_threads(m_nNumParallelThreads) schedule(guided, 10)
        for (int i = 0; i < m_nParticles; i++)
        {
            const int nThreadNumber = omp_get_thread_num();
            // update model (calculate forward kinematics)
            UpdateModel(i, nThreadNumber);

            // evaluate likelihood function (compare edges,...)
            pi[i] = CalculateProbability(i, nThreadNumber);
        }

        CalculateFinalProbabilities();

        c_total = 0;
        for (int i = 0; i < m_nParticles; i++)
        {
            c[i] = c_total;
            c_total += pi[i];
            if (c_total < 0)
            {
                ARMARX_WARNING_S << "CParticleFilterFrameworkParallelized::ParticleFilter(): Error: double values has overrun!";
            }
        }

        // normalize
        const double factor = 1 / c_total;
        for (int i = 0; i < m_nParticles; i++)
        {
            pi[i] *= factor;
        }

        CalculateMean();

        GetMeanConfiguration(pResultMeanConfiguration);

        m_nParticles = nRealNumParticles;

        return CalculateProbabilityForConfiguration(pResultMeanConfiguration);
    }
}
