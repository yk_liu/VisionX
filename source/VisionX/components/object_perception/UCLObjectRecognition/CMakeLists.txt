armarx_component_set_name("UCLObjectRecognition")

find_package(OpenCV QUIET)
armarx_build_if(OpenCV_FOUND "OpenCV not available")
if(OpenCV_FOUND)
    include_directories(SYSTEM ${OpenCV_INCLUDE_DIRS})
endif()

find_package(IVT COMPONENTS ivt ivtopencv QUIET)
armarx_build_if(IVT_FOUND "IVT not available")
if(IVT_FOUND)
    include_directories(${IVT_INCLUDE_DIRS})
endif()

set(COMPONENT_LIBS
    VisionXObjectPerception
    ArmarXCoreInterfaces
    ArmarXCore
    #${OpenCV_LIBRARIES}
    opencv_core
    ${PYTHON_LIBRARIES}
    ${Boost_PYTHON_LIBRARY}
)

set(SOURCES UCLObjectRecognition.cpp)
set(HEADERS UCLObjectRecognition.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
#add_subdirectory(test)
