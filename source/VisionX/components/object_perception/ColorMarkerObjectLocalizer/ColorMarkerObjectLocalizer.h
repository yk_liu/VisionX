﻿/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Component
* @author    Kai Welke <welke at kit dot edu>
* @copyright  2013 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// VisionX
#include <VisionX/components/object_perception/ObjectLocalizerProcessor.h>

// IVT
#include <Color/ColorParameterSet.h>
#include <SegmentableRecognition/SegmentableRecognition.h>
#include <Visualizer/OpenGLVisualizer.h>


#include <VirtualRobot/VirtualRobot.h>

// global forward declarations
class CByteImage;
class CGLContext;

namespace visionx
{
    // shared pointer type definitions
    using CSegmentableRecognitionPtr = std::shared_ptr<CSegmentableRecognition>;
    using CColorParameterSetPtr = std::shared_ptr<CColorParameterSet>;

    // properties of ColorMarkerObjectLocalizer
    class ColorMarkerObjectLocalizerPropertyDefinitions:
        public ObjectLocalizerProcessorPropertyDefinitions
    {
    public:
        ColorMarkerObjectLocalizerPropertyDefinitions(std::string prefix):
            ObjectLocalizerProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ColorParameterFile", "VisionX/examples/colors.txt", "The color parameter file configures the colors used for segmentable recognition (usually colors.txt)");
            defineOptionalProperty<float>("MinPixelsPerRegion", 4, "Minimum number of pixels per region for detecting a uniformly colored object");
            defineOptionalProperty<float>("MaxEpipolarDistance", 4, "Maximum epipolar line distance allowed for a valid 3D recognition result");
            defineOptionalProperty<int>("NumObjectMarker", 4, "Number of markers");
            defineOptionalProperty<int>("Hue", 4, "Hue value of object marker");
            defineOptionalProperty<int>("HueTolerance", 4, "Hue tolerance value of object marker");
            defineOptionalProperty<int>("MinSaturation", 0, "Minimum saturation");
            defineOptionalProperty<int>("MaxSaturation", 255, "Maximum saturation");
            defineOptionalProperty<int>("MinValue", 0, "Minimum value");
            defineOptionalProperty<int>("MaxValue", 255, "Maximum value");
            defineOptionalProperty<std::string>("MarkerColor", "Yellow3", "Marker Color");
            defineOptionalProperty<std::string>("MarkeredObjectName", "MarkeredObject", "Name of the object");
        }
    };

    /**
     * ColorMarkerObjectLocalizer uses CSegmentableRecognition from IVT to recognize and localize single-colored objects based on their color and shape.
     * The object data is read from PriorKnowledge and CommonStorage via MemoryX.
     * The object localization is invoked automatically by the working memory if the object has been requested there.
     *
     */
    class ColorMarkerObjectLocalizer:
        virtual public ObjectLocalizerProcessor
    {
    public:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new ColorMarkerObjectLocalizerPropertyDefinitions(getConfigIdentifier()));
        }

        /**
        * @see Component::getDefaultName()
        */
        std::string getDefaultName() const override
        {
            return "ColorMarkerObjectLocalizer";
        }

        ColorMarkerObjectLocalizer();
        ~ColorMarkerObjectLocalizer() override;

    protected:
        /**
         * @see ObjectLocalizerProcessor::onInitObjectLocalizerProcessor()
         */
        void onInitObjectLocalizerProcessor() override;

        /**
         * Initializes the CSegmentableRecognition
         *
         * @see ObjectLocalizerProcessor::onConnectObjectLocalizerProcessor()
         */
        void onConnectObjectLocalizerProcessor() override;

        /**
         * @see ObjectLocalizerProcessor::onExitObjectLocalizerProcessor()
         */
        void onExitObjectLocalizerProcessor() override;

        /**
         * Initializes segmentable recognition
         *
         * @return success
         */
        bool initRecognizer() override;

        /**
         * localizes object markers instances
         *
         * @param objectClassNames names of the class to localize
         * @param cameraImages the two input images
         * @param resultImages the two result images. are provided if result images are enabled.
         *
         * @return list of object instances
         */
        memoryx::ObjectLocalizationResultList localizeObjectClasses(const std::vector<std::string>& objectClassNames, CByteImage** cameraImages, armarx::MetaInfoSizeBasePtr imageMetaInfo, CByteImage** resultImages) override;
        bool addObjectClass(const memoryx::EntityPtr& objectClassEntity, const memoryx::GridFileManagerPtr& fileManager) override;

    private:
        // calculates recognition certainty
        float calculateRecognitionCertainty(const std::string& objectClassName, const Object3DEntry& entry);
        void visualizeResults(const Object3DList& objectList, CByteImage** resultImages);

        // pointer to IVT segmentable recognition
        CSegmentableRecognitionPtr segmentableRecognition;
        std::shared_ptr<CGLContext> contextGL;
        std::shared_ptr<COpenGLVisualizer> m_pOpenGLVisualizer;

        // settings
        float minPixelsPerRegion;
        float maxEpipolarDistance;
        unsigned int numObjectMarker;
        int hue;
        int hueTol;
        int minS;
        int maxS;
        int minV;
        int maxV;
        std::string markeredObjectName;
        CColorParameterSetPtr colorParameters;
        ObjectColor objectMarkerColor;

        VirtualRobot::RobotPtr remoteRobot;

        // remember colors of objects (not stored in IVT viewdatabase)
        std::map<std::string, ObjectColor> objectColors;
    };
}
