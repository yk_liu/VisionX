
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore FaceRecognition)
 
armarx_add_test(FaceRecognitionTest FaceRecognitionTest.cpp "${LIBS}")