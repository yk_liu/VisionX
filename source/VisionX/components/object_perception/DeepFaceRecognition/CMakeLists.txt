armarx_component_set_name("DeepFaceRecognition")
set(COMPONENT_LIBS
    VisionXObjectPerception
    ${PYTHON_LIBRARIES}
    ${Boost_PYTHON_LIBRARY}
)
set(SOURCES DeepFaceRecognition.cpp)
set(HEADERS DeepFaceRecognition.h)
armarx_add_component("${SOURCES}" "${HEADERS}")
