#pragma once
//#define DEMO_IMAGE "../../../Hiwi/Database/Ventil/Valve5.bmp"
#define DEMO_IMAGE "../../../../bmp/frame0206.bmp"
//#define DEMO_IMAGE "../../../../bmp/frame0285.bmp"
//#define DEMO_IMAGE "../../../../bmp/frame0205.bmp"
//#define DEMO_IMAGE "../../../../bmp/frame0259.bmp"
//#define DEMO_IMAGE "../../../../bmp/frame0267.bmp"
//#define DEMO_IMAGE "../../../../bmp/frame0279.bmp"

//#define DEMO_IMAGE "../../../../bmp/frame0022.bmp"
//#define DEMO_IMAGE "../../../../bmp/frame0082.bmp"


// ****************************************************************************
// Includes
// ****************************************************************************
#include <Interfaces/MainWindowInterface.h>
#include <Interfaces/MainWindowEventInterface.h>
#include <VideoCapture/BitmapCapture.h>
#include <Helpers/helpers.h>

#include <Image/ByteImage.h>
#include <Image/FloatImage.h>
#include <Image/IntImage.h>
#include <Image/ShortImage.h>
#include <Image/ImageProcessor.h>
#include <Image/PrimitivesDrawer.h>
#include <Math/Vecd.h>
#include <Classification/NearestNeighbor.h>



#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <stdio.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <unistd.h>
#include <utility>
#include <limits>

//calculate the HoG descriptor for training Data & test data
//build the classifier
//find the ventil in an image
class HoG
{

public:
    HoG(int numOfBing = 16, int sampleTime = 500, int numOfHistory = 2, int minRandomWindow = 20, int randomWindowScaleFloor = 1, int randomWindowScaleCeil = 2);
    void loadTrainingData(std::string path);
    void findSalientRegions(const CByteImage* origin, CFloatImage* saliencyImage);
    void setParameters(bool useHough, bool useHoG);

private:


    bool hogDescriptor(CByteImage* origin, CFloatImage* outImage, CFloatImage* amplitude);
    void calculateHist(int range, int bin, CFloatImage* inputImg, std::vector<float>* output, CFloatImage* amplitude);
    void histogramNorm(std::vector<float>* input, std::vector<float>* normOutput);
    float distanceBetweenPoints(std::vector<float> firstPoint, std::vector<float> secondPoint, int metrics);
    //void findThreeNearstNeighbors(std::vector<float> testWindow, std::vector<std::vector<float> > trainingHistTable, int lable, std::pair <std::vector<float>, int>* nearstNeighbor, std::pair <std::vector<float>, int>* secondNeighbor, std::pair <std::vector<float>, int>* thirdNeighbor, float* nearstDistance, float* secondDistance, float* thirdDistance);
    void findThreeNearstNeighbors(std::vector<float> testWindow, std::vector<std::vector<float> > trainingHistTable, int lable, std::pair <std::vector<float>*, int>* nearstNeighbor, std::pair <std::vector<float>*, int>* secondNeighbor, std::pair <std::vector<float>*, int>* thirdNeighbor, float* nearstDistance, float* secondDistance, float* thirdDistance);
    bool knn(std::vector<std::vector<float> > trainingHistTable, std::vector<std::vector<float> > trainingHistTableNegative, std::vector<float> testWindow);
    void preprocess(const CByteImage* origin, CByteImage* outImage);
    void histogramRotationInvariant(std::vector<float>* inputHist, std::vector<float>* outputHist);


    void doLoadTrainingData(std::string path, std::vector<std::vector<float> >& histTable, int scaling);

    std::vector<std::vector<float> > trainingHistTable;
    std::vector<std::vector<float> > trainingHistTableNegative;
    std::vector<std::vector<float> > samplHistTable;
    int numberOfBins;
    int sampleTimes;
    int numberOfHistory;
    int minimumRandomWindow;
    int randomWindowScaleRateFloor;
    int randomWindowScaleRateCeil;
    //    int scaleRateFloor;
    //    int scaleRateCeil;


    bool useHoughDetector;
    bool useHoGDetector;
};

