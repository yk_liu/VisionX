#pragma once

#include <Image/ByteImage.h>
#include <Image/ImageProcessor.h>
#include <Image/PrimitivesDrawer.h>
#include <Helpers/helpers.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// ****************************************************************************
// Defines
// ****************************************************************************

class CHoughCircles
{
public:
    CHoughCircles(int CannyLowThreshold = 50, int CannyHighThreshold = 200, int CirclesToExtract = 1, int minRadius = 20, int maxRadius = 100);
    void HoughSaliency(CByteImage* origin, CByteImage* saliencyImage, int sampleWindowsize, int width, int height, int windowCenterX, int windowCenterY);
    void openCVHoughSaliency(CByteImage* origin, CByteImage* saliencyImage, int sampleWindowsize, int width, int height, int windowCenterX, int windowCenterY);
private:
    // private attributes
    int m_nCannyLowThreshold, m_nCannyHighThreshold;
    int m_nMinRadius, m_nMaxRadius;
    int m_nCirclesToExtract;
};
