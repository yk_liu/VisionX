armarx_component_set_name("StreamImageProvider")
set(COMPONENT_LIBS StreamReceiver ArmarXCoreInterfaces ArmarXCore VisionXCore)
set(SOURCES StreamImageProvider.cpp)
set(HEADERS StreamImageProvider.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)

