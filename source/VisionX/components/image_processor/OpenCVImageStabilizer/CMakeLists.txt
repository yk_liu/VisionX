armarx_component_set_name("OpenCVImageStabilizer")

set(COMPONENT_LIBS ArmarXCoreObservers ArmarXCore VisionXCore VisionXTools VisionXInterfaces)

set(SOURCES OpenCVImageStabilizer.cpp)
set(HEADERS OpenCVImageStabilizer.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)
