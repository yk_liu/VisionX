
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore CropRobotFromImage)
 
armarx_add_test(CropRobotFromImageTest CropRobotFromImageTest.cpp "${LIBS}")
