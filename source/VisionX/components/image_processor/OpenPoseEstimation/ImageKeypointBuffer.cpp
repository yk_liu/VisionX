/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    <PACKAGE_NAME>::<CATEGORY>::ImageKeypointBuffer
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ImageKeypointBuffer.h"
#include <Image/ImageProcessor.h>

using namespace armarx;

ImageKeypointBuffer::ImageKeypointBuffer(const visionx::ImageProviderInfo& imageProviderInfo)
{
    this->_imageProviderInfo = imageProviderInfo;
}

ImageKeypointBuffer::~ImageKeypointBuffer()
{
    _buffer.clear();
}

bool ImageKeypointBuffer::addRGBImage(CByteImage* rgbImage, long timestamp)
{
    ScopedLock lock(_bufferMutex);
    ImageKeypointTriplePtr triple = ensureTriple(timestamp);
    triple->rgbImage = new CByteImage(_imageProviderInfo.imageFormat.dimension.width, _imageProviderInfo.imageFormat.dimension.height, CByteImage::eRGB24);
    ::ImageProcessor::CopyImage(rgbImage, triple->rgbImage);

    return isComplete(timestamp);
}

bool ImageKeypointBuffer::addDepthImage(CByteImage* depthImage, long timestamp)
{
    ScopedLock lock(_bufferMutex);
    ImageKeypointTriplePtr triple = ensureTriple(timestamp);
    triple->depthImage = new CByteImage(_imageProviderInfo.imageFormat.dimension.width, _imageProviderInfo.imageFormat.dimension.height, CByteImage::eRGB24);
    ::ImageProcessor::CopyImage(depthImage, triple->depthImage);

    return isComplete(timestamp);
}

bool ImageKeypointBuffer::addKeypoints(KeypointManagerPtr keypoints, long timestamp)
{
    ScopedLock lock(_bufferMutex);
    ImageKeypointTriplePtr triple = ensureTriple(timestamp);
    triple->keypoints.reset(new KeypointManager(*keypoints));

    return isComplete(timestamp);
}

ImageKeypointBuffer::ImageKeypointTriplePtr ImageKeypointBuffer::getTripleAtTimestamp(long timestamp, bool deleteOlderTriples)
{
    ScopedLock lock(_bufferMutex);
    ImageKeypointTriplePtr triple = ensureTriple(timestamp);
    if (triple->keypoints && triple->depthImage && triple->rgbImage)
    {
        std::map<long, ImageKeypointBuffer::ImageKeypointTriplePtr>::iterator it = _buffer.erase(_buffer.find(timestamp));

        if (deleteOlderTriples)
        {
            deleteOlderEntries(it);
        }
        return triple;
    }
    else
    {
        return ImageKeypointTriplePtr();
    }
}

std::pair<long, ImageKeypointBuffer::ImageKeypointTriplePtr> ImageKeypointBuffer::getOldestTriple()
{
    ScopedLock lock(_bufferMutex);
    if (_buffer.size() > 0)
    {
        auto pair = *_buffer.begin();
        _buffer.erase(_buffer.begin());
        return std::move(pair);
    }
    else
    {
        return std::make_pair(0, ImageKeypointTriplePtr());
    }
}

std::pair<long, ImageKeypointBuffer::ImageKeypointTriplePtr> ImageKeypointBuffer::getOldestCompleteTriple(bool deleteOlderTriples)
{
    ScopedLock lock(_bufferMutex);
    // Iterate from the beginning over all entries until a complete one is found
    std::map<long, ImageKeypointBuffer::ImageKeypointTriplePtr>::iterator it = _buffer.begin();
    while (!isComplete((*it).first))
    {
        it++;
    }
    auto pair = *it;

    if (deleteOlderTriples)
    {
        deleteOlderEntries(it);
    }
    return std::move(pair);
}

void ImageKeypointBuffer::clear()
{
    // remove all entires
    _buffer.clear();
}

ImageKeypointBuffer::ImageKeypointTriplePtr ImageKeypointBuffer::ensureTriple(long timestamp)
{
    if (_buffer.count(timestamp) == 0)
    {
        _buffer.insert(std::make_pair(timestamp, std::make_shared<ImageKeypointTriple>()));
    }
    return _buffer.at(timestamp);
}

bool ImageKeypointBuffer::isComplete(long timestamp)
{
    ImageKeypointTriplePtr triple = ensureTriple(timestamp);
    return (triple->keypoints && triple->depthImage && triple->rgbImage) ? true : false;
}

void ImageKeypointBuffer::deleteOlderEntries(std::map<long, ImageKeypointBuffer::ImageKeypointTriplePtr>::iterator it)
{
    if (it == _buffer.end())
    {
        _buffer.clear();
    }
    else
    {
        _buffer.erase(_buffer.begin(), it);
    }
}
