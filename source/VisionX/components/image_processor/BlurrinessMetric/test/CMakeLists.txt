
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore BlurrinessMetric)
 
armarx_add_test(BlurrinessMetricTest BlurrinessMetricTest.cpp "${LIBS}")
