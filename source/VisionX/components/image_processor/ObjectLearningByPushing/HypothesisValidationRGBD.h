/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include "ObjectLearningByPushingDefinitions.h"
#include "ObjectHypothesis.h"


class CCalibration;
class CByteImage;
class CPointCloudRegistration;


class CHypothesisValidationRGBD
{
public:
    static void ValidateInitialHypotheses(const std::vector<CHypothesisPoint*>& aOldDepthMapPoints, const std::vector<CHypothesisPoint*>& aNewDepthMapPoints, const CByteImage* pForegroundImage,
                                          const CByteImage* pHSVImage, const CCalibration* calibration, const Vec3d upwardsVector, CObjectHypothesisArray* pObjectHypotheses, CObjectHypothesisArray* pConfirmedHypotheses);

    static void RevalidateHypotheses(const std::vector<CHypothesisPoint*>& aNewDepthMapPoints, const CByteImage* pForegroundImage,
                                     const CByteImage* pHSVImage, const CCalibration* calibration, const Vec3d upwardsVector, CObjectHypothesisArray* pObjectHypotheses);

    static void GenerateAndAddForegroundHypotheses(const std::vector<CHypothesisPoint*>& aOldDepthMapPoints, const CByteImage* pForegroundImage,
            const CCalibration* calibration, CObjectHypothesisArray* pObjectHypotheses);

    static bool ValidateHypothesis(CPointCloudRegistration* pPointCloudRegistration, const std::vector<CHypothesisPoint*>& aNewDepthMapPoints, const std::vector<CHypothesisPoint*>& aForegroundPoints, const CByteImage* pForegroundImage,
                                   const CByteImage* pHSVImage, const CCalibration* calibration, const Vec3d upwardsVector, CObjectHypothesis* pObjectHypothesis, CObjectHypothesis*& pConfirmedHypothesis,
                                   const int nEffortLevel = OLP_EFFORT_POINTCLOUD_MATCHING);

    static CHypothesisPoint* FuseTwoPoints(CHypothesisPoint* p1, CHypothesisPoint* p2);
};


