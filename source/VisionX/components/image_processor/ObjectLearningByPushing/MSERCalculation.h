/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "ObjectHypothesis.h"
#include <ArmarXCore/core/logging/Logging.h>

class CStereoCalibration;
class CByteImage;

class CMSERCalculation
{
public:
    CMSERCalculation(void);
    ~CMSERCalculation(void);

    static void FindMSERs2D(const CByteImage* pRGBImage, const CByteImage* pHSVImage, std::vector<CMSERDescriptor*>& aMSERDescriptors);
    static void FindMSERs3D(const CByteImage* pByteImageLeft, const CByteImage* pByteImageRight, CStereoCalibration* pStereoCalibration,
                            const float fToleranceFactor, std::vector<CMSERDescriptor3D*>& aRegions3D);
    static void StereoMatchMSERs(const std::vector<CMSERDescriptor*>& aMSERDescriptorsLeft, const std::vector<CMSERDescriptor*>& aMSERDescriptorsRight,
                                 CStereoCalibration* pStereoCalibration, const float fToleranceFactor, std::vector<CMSERDescriptor3D*>& aRegions3D);

    static CMSERDescriptor* CreateMSERDescriptor(std::vector<Vec2d>* aPoints2D, const CByteImage* pHSVImage);
    static CMSERDescriptor3D* CreateMSERDescriptor3D(CMSERDescriptor* pMSERLeft, CMSERDescriptor* pMSERRight, CStereoCalibration* pStereoCalibration);

    static float HistogramDistanceL2(CMSERDescriptor* pDescriptor1, CMSERDescriptor* pDescriptor2);
    static float HistogramDistanceChi2(CMSERDescriptor* pDescriptor1, CMSERDescriptor* pDescriptor2);

    static void SortQuadrangleCorners(Vec2d* pCorners, Vec2d& vUp, Vec2d& vLow, Vec2d& vLeft, Vec2d& vRight);
    static void CreateWeightedHueHistogramWithinQuadrangle(Vec2d vUp, Vec2d vLow, Vec2d vLeft, Vec2d vRight, const CByteImage* pHSVImage,
            float* pHistogram, const int nHistogramSize, int& nSaturationSum);

    static void GetMeanAndCovariance2D(std::vector<Vec2d>& aPoints2D, Vec2d& vMean, Mat2d& mCovariance);
    static void GetEigenvaluesAndEigenvectors2D(Mat2d mMatrix2D, float& fLambda1, float& fLambda2, Vec2d& vE1, Vec2d& vE2);

    static void CreateHSVHistograms(CMSERDescriptor* pDescriptor, const CByteImage* pHSVImage);
};
