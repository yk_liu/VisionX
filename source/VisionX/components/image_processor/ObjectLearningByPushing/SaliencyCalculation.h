/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "ObjectHypothesis.h"

class CCalibration;
class CByteImage;

namespace CSaliencyCalculation
{
    void FindLocalMaxima(const std::vector<Vec3d>& aPoints3D, const std::vector<Vec2d>& aPointsInImage, std::vector<Vec3d>& aMaxima, const int nBinSizeInPx, CByteImage* pMaximumnessImage);
    void FindLocalMaxima(const std::vector<CHypothesisPoint*>& aPoints, const Mat3d mCameraToWorldRotation, const Vec3d vCameraToWorldTranslation,
                         const CCalibration* calibration, std::vector<Vec3d>& aMaxima, CByteImage* pMaximumnessImage, const int nBinSizeInPx = 16);

    struct CComplexNumber
    {
        double r, i;
    };
    CComplexNumber MultCompl(const CComplexNumber a, const CComplexNumber b);
    CComplexNumber ExpComplImagOnly(const float a);

    void FourierTransformation(const CByteImage* pGrayImage, CComplexNumber* pTransformed);
    void InverseFourierTransformation(CComplexNumber* pTransformed, CByteImage* pGrayImage);
    void SubstractAveragedLogPowerSpectrum(CComplexNumber* pTransformed);
    void FindSalientRegionsHou(const CByteImage* pImageRGB, CByteImage* pSaliencyImage);
    void FindSalientRegionsAchanta(const CByteImage* pImageRGB, CByteImage* pSaliencyImage);

    void ConvertRGB2Lab(const CByteImage* pImageRGB, CByteImage* pImageLab);
}

