/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RTABMapRegistration
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>


#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/interface/core/RobotState.h>

#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/libraries/memorytypes/entity/AgentInstance.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>


#include <VisionX/interface/components/RTABMapInterface.h>
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>
#include <VisionX/tools/TypeMapping.h>
#include <VisionX/tools/ImageUtil.h>

#include <Eigen/Geometry>


#include <opencv2/opencv.hpp>
#include <Image/IplImageAdaptor.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <pcl/io/pcd_io.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>

#include <rtabmap/core/Rtabmap.h>
#include <rtabmap/core/RtabmapThread.h>

#include <rtabmap/core/CameraEvent.h>
#include <rtabmap/core/Odometry.h>

#include <rtabmap/core/OdometryThread.h>
#include <rtabmap/core/OdometryEvent.h>


#include <rtabmap/utilite/UEventsManager.h>
#include <rtabmap/utilite/UEventsHandler.h>
#include <rtabmap/utilite/ULogger.h>

#pragma GCC diagnostic pop

namespace armarx
{
    /**
     * @class RTABMapRegistrationPropertyDefinitions
     * @brief
     */
    class RTABMapRegistrationPropertyDefinitions:
        public visionx::CapturingPointCloudProviderPropertyDefinitions
    {
    public:
        RTABMapRegistrationPropertyDefinitions(std::string prefix):
            visionx::CapturingPointCloudProviderPropertyDefinitions(prefix)
        {
            // defineRequiredProperty<std::string>("agentName", "name of the agent for which the sensor values are provided");

            defineOptionalProperty<std::string>("agentName", "RTABMap", "");
            defineOptionalProperty<std::string>("providerName", "OpenNIPointCloudProvider", "");
            //defineOptionalProperty<std::string>("sourceFrameName", "DepthCamera", "the robot node to use as source coordinate system for the captured point clouds");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the working memory");
            defineOptionalProperty<bool>("provideGlobalPointCloud", false,  "provide global point cloud if set. otherwise only the current view");
            defineOptionalProperty<float>("MinDistance", 0.02, "minimum z-axis distance in meters");
            defineOptionalProperty<float>("MaxDistance", 5.00, "maximum z-axis distance in meters");
        }
    };

    /**
     * @class RTABMapRegistration
     *
     * @ingroup VisionX-Components
     * @brief A brief description
     *
     *
     * Detailed Description
     */
    class RTABMapRegistration :
        virtual public visionx::RTABMapInterface,
        virtual public visionx::CapturingPointCloudProvider,
        virtual public visionx::ImageProcessor,
        virtual public UEventsHandler
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "RTABMapRegistration";
        }

    protected:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitCapturingPointCloudProvider() override;
        void onExitCapturingPointCloudProvider() override;
        void onStartCapture(float framesPerSecond) override;
        void onStopCapture() override;
        bool doCapture() override;

        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onExitImageProcessor() override;
        void process() override;

        void onInitComponent() override
        {
            CapturingPointCloudProvider::onInitComponent();
            ImageProcessor::onInitComponent();
        }

        void onConnectComponent() override
        {
            ImageProcessor::onConnectComponent();
            CapturingPointCloudProvider::onConnectComponent();
        }

        void onDisconnectComponent() override
        {
            CapturingPointCloudProvider::onDisconnectComponent();
            ImageProcessor::onDisconnectComponent();
        }

        void onExitComponent() override
        {
            CapturingPointCloudProvider::onExitComponent();
            ImageProcessor::onExitComponent();
        }

        bool handleEvent(UEvent* event) override;

        visionx::MetaPointCloudFormatPtr getDefaultPointCloudFormat() override;


    private:

        bool extractPointCloud(rtabmap::SensorData sensorData, Eigen::Matrix4f transform, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& tempCloudPtr);
        void updatePosition(Eigen::Matrix4f pose);
        void processData();

        std::string agentName;
        std::string providerName;
        std::string sourceFrameName;
        bool provideGlobalPointCloud;

        rtabmap::Rtabmap* rtabmap;
        rtabmap::RtabmapThread* rtabmapThread;
        rtabmap::OdometryThread* odomThread;
        rtabmap::CameraModel cameraModel;


        CByteImage** images;
        armarx::MetaInfoSizeBasePtr imageMetaInfo;
        int imageId;

        Eigen::Matrix4f initialCameraPose;

        boost::mutex RTABDataMutex;
        bool hasNewData;
        boost::condition dataCondition;

        void requestGlobalPointCloud();
        std::map<int, rtabmap::Signature> signatures;
        std::map<int, rtabmap::Transform> poses;

        boost::mutex resultPointCloudMutex;
        boost::condition resultCondition;
        bool hasNewResultPointCloud;
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr resultPointCloudPtr;

        pcl::PassThrough<pcl::PointXYZRGBA> passThroughFilter;

        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        memoryx::WorkingMemoryInterfacePrx workingMemory;
        memoryx::AgentInstancesSegmentBasePrx agentInstancesSegment;

        memoryx::AgentInstancePtr agentInstance;
        std::string agentInstanceId;

        armarx::PeriodicTask<RTABMapRegistration>::pointer_type updateTask;
    };
}

