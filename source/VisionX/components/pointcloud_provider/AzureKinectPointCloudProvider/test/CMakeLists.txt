
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore AzureKinectPointCloudProvider)
 
armarx_add_test(AzureKinectPointCloudProviderTest AzureKinectPointCloudProviderTest.cpp "${LIBS}")
