armarx_component_set_name("MultiSensePointCloudProvider")

find_package(multisense_lib QUIET)
armarx_build_if(multisense_lib_FOUND "LibMultiSense not available")

set(COMPONENT_LIBS
    VisionXPointCloud
    VisionXCore
    ${PCL_FILTERS_LIBRARY}
    ${multisense_lib_LIBRARIES}
)

set(SOURCES MultiSensePointCloudProvider.cpp)
set(HEADERS MultiSensePointCloudProvider.h)

armarx_add_component("${SOURCES}" "${HEADERS}")
add_dependencies(MultiSensePointCloudProvider VisionXScripts)
if(multisense_lib_FOUND)
    target_include_directories(MultiSensePointCloudProvider SYSTEM PUBLIC ${multisense_lib_INCLUDE_DIRS})
endif()

# add unit tests
add_subdirectory(test)
