//
// Created by christoph on 22.08.19.
//

#ifndef VISIONX_KINECTV1DEVICE_HPP
#define VISIONX_KINECTV1DEVICE_HPP

#include <libfreenect.hpp>
#include <boost/thread/mutex.hpp>
#include <vector>



class KinectV1Device : public Freenect::FreenectDevice
{
public:
    KinectV1Device(freenect_context* _ctx, int _index)
        : Freenect::FreenectDevice(_ctx, _index),
          m_buffer_video(freenect_find_video_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_VIDEO_RGB).bytes),
          m_buffer_depth(freenect_find_depth_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_DEPTH_REGISTERED).bytes / 2),
          m_new_rgb_frame(false), m_new_depth_frame(false)
    {
        setDepthFormat(FREENECT_DEPTH_REGISTERED);
    }

    // Do not call directly, even in child
    void VideoCallback(void* _rgb, uint32_t timestamp)
    {
        boost::mutex::scoped_lock lock(m_rgb_mutex);
        uint8_t* rgb = static_cast<uint8_t*>(_rgb);
        copy(rgb, rgb + getVideoBufferSize(), m_buffer_video.begin());
        m_new_rgb_frame = true;
    }

    // Do not call directly, even in child
    void DepthCallback(void* _depth, uint32_t timestamp)
    {
        boost::mutex::scoped_lock lock(m_depth_mutex);
        uint16_t* depth = static_cast<uint16_t*>(_depth);
        copy(depth, depth + getDepthBufferSize() / 2, m_buffer_depth.begin());
        m_new_depth_frame = true;
    }

    bool getRGB(std::vector<uint8_t>& buffer)
    {
        boost::mutex::scoped_lock lock(m_rgb_mutex);

        if (!m_new_rgb_frame)
        {
            return false;
        }

        buffer.swap(m_buffer_video);
        m_new_rgb_frame = false;

        return true;
    }

    bool getDepth(std::vector<uint16_t>& buffer)
    {
        boost::mutex::scoped_lock lock(m_depth_mutex);

        if (!m_new_depth_frame)
        {
            return false;
        }

        buffer.swap(m_buffer_depth);
        m_new_depth_frame = false;

        return true;
    }

private:
    mutable boost::mutex m_rgb_mutex;
    mutable boost::mutex m_depth_mutex;
    std::vector<uint8_t> m_buffer_video;
    std::vector<uint16_t> m_buffer_depth;
    bool m_new_rgb_frame;
    bool m_new_depth_frame;
};

#endif //VISIONX_KINECTV1DEVICE_HPP
