/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     David Gonzalez (david dot gonzalez at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// VisionXCore
#include <VisionX/core/CapturingImageProvider.h>

// STL
#include <string>
#include <map>
#include <float.h>

// OpenNI
#include <OpenNI.h>

namespace visionx
{
    class OpenNIPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {

    public:

        OpenNIPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<ImageDimension>("VideoMode", ImageDimension(320,  240), "Image resolution")
            .setCaseInsensitive(true)
            .map("320x240",     ImageDimension(320,  240))
            .map("640x480",     ImageDimension(640,  480));

            defineOptionalProperty<ImageType>("ImageType", eColoredPointsScan, "Image type: RGB, Points, Points with RGB")
            .setCaseInsensitive(true)
            //.map("ePointsScan", ePointsScan)
            .map("ColoredPointsScan", eColoredPointsScan);

            defineOptionalProperty<float>("FrameRate", 30.0f, "Frames per second")
            .setMatchRegex("\\d+(.\\d*)?")
            .setMin(15.0f)
            .setMax(30.0f);

            // Camera URI
            /*verify this lexical-rule*/
            defineOptionalProperty<std::string>("DeviceURI", "ANY", "The URI from OpenNI API").setCaseInsensitive(true);

        }
    };


    /**
     * OpenNI image provider captures 3D-points and/or color(s) images from a single device and
     * supports the following image transmission formats:
     *
     *  - RGB: 3, bytes per Pixel
     *  - PointsScan (X,Y,Z): 3, floats per Pixel
     *  - ColoredPointsScan, Geometrically registered (X,Y,Z,R,G,B): 3 floats + 3 bytes per Pixel
     *
     * \componentproperties
     * \prop VisionX.OpenNIImageProvider.CameraURI: The particular camera URI tha wanst to be addressed.
                                                    If not set the first device on the USBus will be opened .
     * \prop VisionX.OpenNIImageProvider.VideoMode: Defines the camera
     *       resolution.
     *          - 320x240 (default)
     *          - 640x480
     * \prop VisionX.OpenNIImageProvider.ImageType: Specifies how image type is composed.
     *         Possible values:
     *          - rgb
     *          - PointsScan
     *          - ColoredPointsScan
     * \prop VisionX.OpenNIImageProvider.FrameRate: Capture frame rate as
     *       float (default: 30fps)
     */

    class OpenNIImageProvider :
        virtual public visionx::CapturingImageProvider
    {

    public:

        /*NEEDS TO BE UPDATED WITH THE RGB->RGBA Changes
        static CByteImage* Display(const visionx::ColoredPoint3D* pColoredPoint3DBufferBase,const int Width, const int Height,const bool DisplayColor)
        {
            if( (!pColoredPoint3DBufferBase) || (Width<1) || (Height<1) )
            {
                return NULL;
            }

            CByteImage* pDisplayImage = new CByteImage(Width,Height,CByteImage::eRGB24);

            visionx::RGB* pRGB = (visionx::RGB*)(pDisplayImage->pixels);

            const visionx::ColoredPoint3D* pColoredPoint3DBuffer = pColoredPoint3DBufferBase;

            const visionx::ColoredPoint3D* const pEndColoredPoint3DBuffer = pColoredPoint3DBufferBase + (Width * Height);

            if(DisplayColor)
            {
                while(pColoredPoint3DBuffer < pEndColoredPoint3DBuffer)
                {
                    *pRGB++ = pColoredPoint3DBuffer->m_Color;

                    ++pColoredPoint3DBuffer;
                }
            }
            else
            {
                float MinimalDepth = FLT_MAX;

                float MaximalDepth = -FLT_MAX;

                while(pColoredPoint3DBuffer<pEndColoredPoint3DBuffer)
                {
                    if(pColoredPoint3DBuffer->m_Point.m_Z)
                    {

                        if(pColoredPoint3DBuffer->m_Point.m_Z > MaximalDepth)
                            MaximalDepth = pColoredPoint3DBuffer->m_Point.m_Z;


                        if(pColoredPoint3DBuffer->m_Point.m_Z < MinimalDepth)
                            MinimalDepth = pColoredPoint3DBuffer->m_Point.m_Z;

                    }

                    ++pColoredPoint3DBuffer;
                }

                const float DepthRange = MaximalDepth - MinimalDepth;

                if(DepthRange < FLT_EPSILON)
                {
                    memset(pRGB,128,sizeof(visionx::RGB) * Width * Height);

                    return pDisplayImage;
                }

                const float ScalingFactor = 255.0f / DepthRange;

                pColoredPoint3DBuffer = pColoredPoint3DBufferBase;

                 while(pColoredPoint3DBuffer<pEndColoredPoint3DBuffer)
                 {
                     if(pColoredPoint3DBuffer->m_Point.m_Z)
                     {

                        pRGB->m_R =  pRGB->m_G = pRGB->m_B = 255 - int(((pColoredPoint3DBuffer->m_Point.m_Z - MinimalDepth) * ScalingFactor) + 0.5f);

                     }
                     else
                     {
                         pRGB->m_R = 255;

                         pRGB->m_G = pRGB->m_B = 0;
                     }

                     ++pRGB;

                     ++pColoredPoint3DBuffer;
                 }
            }

            return pDisplayImage;
        }*/

        /**
         * @see visionx::ImageProviderBase::onInitImageProvider()
         */
        virtual void onInitCapturingImageProvider();

        /**
         * @see visionx::ImageProviderBase::onExitImageProvider()
         */
        virtual void onExitCapturingImageProvider();

        /**
         * @see visionx::ImageProviderBase::onStartCapture()
         */
        virtual void onStartCapture(float frameRate);

        /**
         * @see visionx::ImageProviderBase::onStopCapture()
         */
        virtual void onStopCapture();

        /**
         * @see visionx::ImageProviderBase::capture()
        */
        virtual bool capture(void** ppImageBuffers);


        /*
        void provideImages(CByteImage** images);


        void provideImages(CFloatImage** images);
        */

        /**
         * @see armarx::Component::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "OpenNIImageProvider";
        }

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions()
        {
            return armarx::PropertyDefinitionsPtr(new OpenNIPropertyDefinitions(getConfigIdentifier()));
        }


    protected:

        /**
         * Camera URI
         */
        std::string uri;

        /**
         * Video dimension data
         */
        visionx::ImageDimension videoDimension;

        /**
         * Capturing frame rate
         */
        float frameRate;

        /**
         * Capturing image type
         */
        visionx::ImageType imageType;

        /**
         * Captured images
         */
        visionx::ColoredPoint3D* coloredPoint3DBuffer;

        /**
         * OpenNI Capture
         */

        struct NormalizedDepthCell
        {
            float nx;
            float ny;
        };

        NormalizedDepthCell* normalizedDepthCells;

        openni::Device deviceOpenNI;

        openni::VideoStream depthStreamOpenNI;

        openni::VideoStream colorStreamOpenNI;

        openni::VideoFrameRef frameOpenNI;

        static bool isBackEndInitialized;

        bool isModuleInitialized;

        bool StartDevice(const char* pDeviceURI, const int Width, const int Height, const int Fps);

        bool CreateDataStructures();

        bool InitializeNormalizedDepthCells();

        bool StopDevice();

        bool DestroyDataStructure();

        bool CaptureColoredPoint();

        bool DispatchDepthFrame();

        bool DispatchColorFrame();

        int EnsureFPS(const int FPS);
    };
}


