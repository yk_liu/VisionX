armarx_component_set_name(OpenNIImageProvider)

find_package(OpenNI QUIET)
armarx_build_if(OpenNI_FOUND "OpenNI not available")

set(COMPONENT_LIBS VisionXInterfaces VisionXTools VisionXCore ArmarXCore ${OpenNI_LIBRARY})

set(SOURCES OpenNIImageProvider.cpp)
set(HEADERS OpenNIImageProvider.h)
armarx_add_component("${SOURCES}" "${HEADERS}")
if(OpenNI_FOUND)
    target_include_directories(OpenNIImageProvider SYSTEM PUBLIC ${OpenNI_INCLUDE_DIRS})
endif()

set(EXE_SOURCES main.cpp OpenNIImageProviderApp.h)
armarx_add_component_executable("${EXE_SOURCES}")
