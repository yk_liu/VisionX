#include "pcl_point_operators.h"


namespace pcl
{

    template <typename PointT>
    static bool neq_xyz(const PointT& lhs, const PointT& rhs)
    {
        return lhs.x != rhs.x || lhs.y != rhs.y || lhs.z != rhs.z;
    }

    template <typename PointT>
    static std::ostream& out_xyz(std::ostream& os, const PointT& rhs)
    {
        return os << "[" << rhs.x << " " << rhs.y << " " << rhs.z << "]";
    }


    template <typename PointT>
    static bool neq_rgba(const PointT& lhs, const PointT& rhs)
    {
        return lhs.r != rhs.r || lhs.g != rhs.g || lhs.b != rhs.b || lhs.a != rhs.a;
    }

    template <typename PointT>
    static std::ostream& out_rgba(std::ostream& os, const PointT& rhs)
    {
        return os << "[" << rhs.r << " " << rhs.g << " " << rhs.b << " " << rhs.a << "]";
    }


    bool operator!=(const PointXYZ& lhs, const PointXYZ& rhs)
    {
        return neq_xyz(lhs, rhs);
    }

    std::ostream& operator<<(std::ostream& os, const PointXYZ& rhs)
    {
        return os << "[" << rhs.x << " " << rhs.y << " " << rhs.z << "]";
    }


    bool operator!=(const PointXYZRGBA& lhs, const PointXYZRGBA& rhs)
    {
        return neq_xyz(lhs, rhs) || neq_rgba(lhs, rhs);
    }

    std::ostream& operator<<(std::ostream& os, const PointXYZRGBA& rhs)
    {
        os << "(";
        out_xyz(os, rhs) << " ";
        return out_rgba(os, rhs) << ")";
    }


    bool operator!=(const PointXYZL& lhs, const PointXYZL& rhs)
    {
        return neq_xyz(lhs, rhs) || lhs.label != rhs.label;
    }

    std::ostream& operator<<(std::ostream& os, const PointXYZL& rhs)
    {
        os << "(";
        return out_xyz(os, rhs) << " " << rhs.label << ")";
    }
}

