armarx_component_set_name("AffordancePipelineVisualization")

find_package(AffordanceKit QUIET)

armarx_build_if(AffordanceKit_FOUND "AffordanceKit not available")
armarx_build_if(PCL_FOUND "PCL not available")

set(COMPONENT_LIBS
    ArmarXCoreObservers
    VisionXInterfaces
    MemoryXMemoryTypes
    RobotAPICore
    AffordanceKitArmarX
    ${PCL_COMMON_LIBRARIES}
)

set(SOURCES AffordancePipelineVisualization.cpp)
set(HEADERS AffordancePipelineVisualization.h)
armarx_add_component("${SOURCES}" "${HEADERS}")
if(PCL_FOUND)
    target_include_directories(AffordancePipelineVisualization SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})
endif()
