armarx_component_set_name("MaskFilterPointCloudProcessor")

set(COMPONENT_LIBS
    RemoteGui

    RobotAPICore
    DebugDrawer

    VisionXCore
    VisionXPointCloud
)

set(SOURCES MaskFilterPointCloudProcessor.cpp)
set(HEADERS MaskFilterPointCloudProcessor.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)
