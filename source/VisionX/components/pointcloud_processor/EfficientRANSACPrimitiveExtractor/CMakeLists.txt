armarx_component_set_name("EfficientRANSACPrimitiveExtractor")

set(COMPONENT_LIBS
    ArmarXCoreComponentPlugins
    ArmarXGuiComponentPlugins
    RobotAPIComponentPlugins
    VisionXPointCloud
    VisionXInterfaces
    ${PCL_FILTERS_LIBRARY}
)

file(GLOB glob_cpp "EfficientRANSAC/*.cpp" 
                   "EfficientRANSAC/MiscLib/*.cpp")
file(GLOB glob_hdr "EfficientRANSAC/*.h"
                   "EfficientRANSAC/MiscLib/*.h"
                   "EfficientRANSAC/GfxTL/*.h")

include_directories("${CMAKE_CURRENT_LIST_DIR}/EfficientRANSAC")
include_directories("${CMAKE_CURRENT_LIST_DIR}/EfficientRANSAC/MiscLib")

set(CMAKE_UNITY_BUILD OFF)

set(SOURCES ${glob_cpp} EfficientRANSACPrimitiveExtractor.cpp)
set(HEADERS ${glob_hdr} EfficientRANSACPrimitiveExtractor.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

target_include_directories(EfficientRANSACPrimitiveExtractor PUBLIC ${MyLib_INCLUDE_DIRS})
target_compile_options(EfficientRANSACPrimitiveExtractor PRIVATE -Wno-unknown-pragmas
                                                                 -Wno-unused-result
                                                                 -Wno-strict-aliasing
                                                                 -Wno-sign-compare
                                                                 -Wno-deprecated-declarations
                                                                 -Wno-sequence-point
                                                                 -Wno-ignored-qualifiers
                                                                 -Wno-pedantic
                                                                 -Wno-error
                                                                 -fpermissive)

# add unit tests
add_subdirectory(test)
