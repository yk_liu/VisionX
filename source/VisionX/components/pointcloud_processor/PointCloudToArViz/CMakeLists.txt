armarx_component_set_name("PointCloudToArViz")


find_package(Simox QUIET)
armarx_build_if(Simox_FOUND "Simox not available")

find_package(PCL 1.8 QUIET)
armarx_build_if(PCL_FOUND "PCL not available")


set(COMPONENT_LIBS
    RobotAPICore RobotAPIComponentPlugins
    VisionXInterfaces VisionXCore VisionXPointCloud
    ${PCL_LIBRARIES}
    VirtualRobot
)

set(SOURCES
    PointCloudToArViz.cpp
)
set(HEADERS
    PointCloudToArViz.h
)


armarx_add_component("${SOURCES}" "${HEADERS}")


if(PCL_FOUND)
    target_include_directories(PointCloudToArViz SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})

    # Causes:
    # [ 53%] Building CXX object source/VisionX/components/pointcloud_processor/PointCloudToArViz/CMakeFiles/PointCloudToArViz.dir/PointCloudToArViz.cpp.o
    # c++: error:  : No such file or directory
    # source/VisionX/components/pointcloud_processor/PointCloudToArViz/CMakeFiles/PointCloudToArViz.dir/build.make:62: recipe for target 'source/VisionX/components/pointcloud_processor/PointCloudToArViz/CMakeFiles/PointCloudToArViz.dir/PointCloudToArViz.cpp.o' failed
    # CMakeFiles/Makefile2:7025: recipe for target 'source/VisionX/components/pointcloud_processor/PointCloudToArViz/CMakeFiles/PointCloudToArViz.dir/all' failed
    # make[2]: *** [source/VisionX/components/pointcloud_processor/PointCloudToArViz/CMakeFiles/PointCloudToArViz.dir/PointCloudToArViz.cpp.o] Error 1
    # make[1]: *** [source/VisionX/components/pointcloud_processor/PointCloudToArViz/CMakeFiles/PointCloudToArViz.dir/all] Error 2
    # make[1]: *** Waiting for unfinished jobs....

    # remove_empty_elements(PCL_DEFINITIONS)
    # target_compile_options(PointCloudToArViz PUBLIC ${PCL_DEFINITIONS})
endif()


#find_package(MyLib QUIET)
#armarx_build_if(MyLib_FOUND "MyLib not available")
# all target_include_directories must be guarded by if(Xyz_FOUND)
# for multiple libraries write: if(X_FOUND AND Y_FOUND)....
#if(MyLib_FOUND)
#    target_include_directories(PointCloudToArViz PUBLIC ${MyLib_INCLUDE_DIRS})
#endif()


armarx_component_set_name("PointCloudToArVizApp")
set(COMPONENT_LIBS PointCloudToArViz)
armarx_add_component_executable(main.cpp)

