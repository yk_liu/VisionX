/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudToArViz
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PointCloudToArViz.h"


#include <pcl/point_types.h>


namespace visionx
{

    PointCloudToArVizPropertyDefinitions::PointCloudToArVizPropertyDefinitions(std::string prefix) :
        visionx::PointCloudProcessorPropertyDefinitions(prefix)
    {
        defineOptionalProperty<int>("ProviderWaitMs", 100,
                                    "Time to wait for each point cloud provider [ms].");
    }


    std::string PointCloudToArViz::getDefaultName() const
    {
        return "PointCloudToArViz";
    }


    void PointCloudToArViz::onInitPointCloudProcessor()
    {
        providerWaitTime = IceUtil::Time::milliSeconds(getProperty<int>("ProviderWaitMs").getValue());
    }


    void PointCloudToArViz::onConnectPointCloudProcessor()
    {
    }


    void PointCloudToArViz::onDisconnectPointCloudProcessor()
    {
    }

    void PointCloudToArViz::onExitPointCloudProcessor()
    {
    }


    void PointCloudToArViz::process()
    {
        // Fetch input clouds.
        for (const std::string& providerName : getPointCloudProviderNames())
        {
            if (waitForPointClouds(providerName, static_cast<int>(providerWaitTime.toMilliSeconds())))
            {
                auto it = cache.find(providerName);
                if (it == cache.end())
                {
                    it = cache.emplace(providerName, armarx::viz::PointCloud("PointCloud")).first;
                }
                armarx::viz::PointCloud& vizCloud = it->second;

                const PointContentType type = getPointCloudFormat(providerName)->type;
                if (visionx::tools::isLabeled(type))
                {
                    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr inputCloud(new pcl::PointCloud<pcl::PointXYZRGBL>);
                    getPointClouds(providerName, inputCloud);
                    vizCloud.pointCloud(*inputCloud);
                }
                else
                {
                    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr inputCloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
                    getPointClouds(providerName, inputCloud);
                    vizCloud.pointCloud(*inputCloud);
                }

                vizCloud.pointSizeInPixels(pointSizeInPixels);

                armarx::viz::Layer layer = arviz.layer(providerName);
                layer.add(vizCloud);
                arviz.commit(layer);
            }
            else
            {
                ARMARX_VERBOSE << "Timeout waiting for point cloud provider " << providerName << ".";
            }
        }
    }


    armarx::PropertyDefinitionsPtr PointCloudToArViz::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs(new PointCloudToArVizPropertyDefinitions(getConfigIdentifier()));

        defs->optional(pointSizeInPixels, "viz.pointSizeInPixels", "The point size in pixels.");

        return defs;
    }

}
