/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudToArViz
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

// The has_member macro from
// > simox/SimoxUtility/meta/has_member_macros/has_member.hpp
// breaks compilation here if boost thread is not already included
#ifdef has_member
#undef has_member
#endif

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>


namespace visionx
{

    /**
     * @class PointCloudToArVizPropertyDefinitions
     * @brief Property definitions of `PointCloudToArViz`.
     */
    class PointCloudToArVizPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        PointCloudToArVizPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-PointCloudToArViz PointCloudToArViz
     * @ingroup VisionX-Components
     *
     * This point cloud processor draws the specified components to ArViz.
     *
     * @class PointCloudToArViz
     * @ingroup Component-PointCloudToArViz
     * @brief Brief description of class PointCloudToArViz.
     *
     * Detailed description of class PointCloudToArViz.
     */
    class PointCloudToArViz :
        virtual public visionx::PointCloudProcessor,
        virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        void onInitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        void onConnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        void onDisconnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        void onExitPointCloudProcessor() override;


        /// @see visionx::PointCloudProcessor::process()
        void process() override;


        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        /// Time waiting for each provider before timeout.
        IceUtil::Time providerWaitTime = IceUtil::Time::milliSeconds(100);

        /// The point size in pixels.
        float pointSizeInPixels = 1;


        /// Cache of constructed and allocated `viz::PointCloud` instances.
        std::map<std::string, armarx::viz::PointCloud> cache;

    };
}

