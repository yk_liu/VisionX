armarx_component_set_name("TabletopSegmentation")
set(COMPONENT_LIBS
    RemoteGui
    RobotAPICore
    VisionXPointCloud
    ${PCL_SEARCH_LIBRARY}
    ${PCL_SEGMENTATION_LIBRARY}
    ${PCL_SURFACE_LIBRARY}
    ${PCL_FILTERS_LIBRARY}
)
set(SOURCES TabletopSegmentation.cpp)
set(HEADERS TabletopSegmentation.h)
armarx_add_component("${SOURCES}" "${HEADERS}")
