armarx_component_set_name("UserAssistedSegmenter")


set(COMPONENT_LIBS 
    VisionXCore
    VisionXPointCloud
    VisionXInterfaces
    
    ${PCL_LIBRARIES}
)

set(SOURCES
    UserAssistedSegmenter.cpp
)
set(HEADERS
    UserAssistedSegmenter.h
)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)
