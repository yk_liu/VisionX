/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <cassert>

//Eigen
#include <Eigen/Core>

#include <ArmarXCore/core/logging/Logging.h>


class IVPointParser
{
public:
    static std::vector<Eigen::Vector3f> fromFile(const std::string& fileName)
    {
        std::vector<Eigen::Vector3f> ps;

        std::ifstream file(fileName.c_str(), std::ifstream::in);

        if (file.is_open())
        {
            std::string word;

            //Find the points
            while (file >> word)
            {
                if (word.find("Coordinate3") != std::string::npos)
                {
                    break;
                }
            }

            //Skip first {
            file >> word;
            //Should be "point"
            file >> word;
            assert(word.find("point") != std::string::npos);

            //Skip next [
            file >> word;

            //Start with the points
            while (true)
            {
                double x, y, z;
                file >> x >> y >> z;

                if (file.fail())
                {
                    break;
                }

                file.get(); //Last comma

                Eigen::Vector3f v;
                v << x, y, z;
                ps.push_back(v);
            }

            file.close();
        }
        else
        {
            ARMARX_WARNING_S << "Could not open IV file to parse points" << armarx::flush;
        }

        return ps;
    }
};

