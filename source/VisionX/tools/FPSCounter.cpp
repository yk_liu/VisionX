/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// *******************************************************
// include
// *******************************************************
#include "FPSCounter.h"
#include <sys/time.h>
#include <cfloat>

namespace visionx
{
    // *******************************************************
    // construction / destruction
    // *******************************************************
    FPSCounter::FPSCounter(int nDelayFrames)
    {
        m_nDelayFrames = nDelayFrames;

        reset();
    }


    // *******************************************************
    // control
    // *******************************************************
    void FPSCounter::reset()
    {
        // init members
        m_nIntervalTime = 0;

        m_fFPS = 0.0f;
        m_nUpdates = 0;
        m_fMinCycleTimeMS = FLT_MAX;
        m_fMaxCycleTimeMS = -FLT_MAX;
    }

    void FPSCounter::update()
    {
        // calculate current cycle time
        int nCycleTime = calculateTimeDiff(m_nCycleSec, m_nCycleUSec, true);

        // update members with time diff
        updateMembers(nCycleTime);

        // increment update counter
        m_nUpdates++;
    }

    void FPSCounter::recalculate()
    {
        // TODO proper implementation
        // calculate current cycle time
        //   int nCycleTime = calculateTimeDiff(m_nCycleSec, m_nCycleUSec, false);

        // update members with time diff
        //    updateMembers(nCycleTime);
    }

    void FPSCounter::assureFPS(float fFrameRate)
    {
        // first run is not valid
        bool bValidTime = (m_nUpdates != 0);
        int nOverallCycleTime = (1.0f / fFrameRate) * 1000000;

        if (bValidTime)
        {
            // calculate target cycle time
            int nWantedUSec = (1.0f / fFrameRate) * 1000000;

            int nTimeDiff;

            // wait until wanted cycle time is reached
            do
            {
                nTimeDiff =  calculateTimeDiff(m_nCycleSec, m_nCycleUSec);
                usleep(1);
            }
            while (nTimeDiff < nWantedUSec);

            // update time stamp
            nOverallCycleTime = calculateTimeDiff(m_nCycleSec, m_nCycleUSec, true);
        }

        calculateTimeDiff(m_nCycleSec, m_nCycleUSec, true);
        updateMembers(nOverallCycleTime);

        // increment update counter
        m_nUpdates++;
    }

    // *******************************************************
    // member access
    // *******************************************************
    bool FPSCounter::getValid()
    {
        return m_nUpdates > m_nDelayFrames;
    }

    int FPSCounter::getUpdates()
    {
        return m_nUpdates;
    }

    float FPSCounter::getFPS()
    {
        if (m_nUpdates <= m_nDelayFrames)
        {
            return 0.0f;
        }

        return m_fFPS;
    }

    float FPSCounter::getMeanCycleTimeMS()
    {
        if (m_nUpdates <= 10)
        {
            return 0.0f;
        }

        float fMeanTimeMS = 0.0f;

        for (int i = 0 ; i < 10 ; i++)
        {
            fMeanTimeMS += m_fLastCycleTimesMS[i];
        }

        return fMeanTimeMS / 10;
    }

    float FPSCounter::getMinCycleTimeMS()
    {
        if (m_nUpdates <= 1)
        {
            return 0.0f;
        }

        return m_fMinCycleTimeMS;
    }

    float FPSCounter::getMaxCycleTimeMS()
    {
        if (m_nUpdates <= 1)
        {
            return 0.0f;
        }

        return m_fMaxCycleTimeMS;
    }

    // *******************************************************
    // private methods
    // *******************************************************
    void FPSCounter::updateMembers(int nCycleTime)
    {
        // first run is not valid
        bool bValidTime = (m_nUpdates != 0);

        // update statistics
        recalculateFPS(nCycleTime);

        if (bValidTime)
        {
            recalculateStats(nCycleTime);
        }
    }

    void FPSCounter::recalculateFPS(int nCycleUSec)
    {
        m_nIntervalTime += nCycleUSec;

        // only recalculate over some time
        if ((m_nUpdates % m_nDelayFrames == 0) && (m_nUpdates != 0))
        {
            // calculate fps
            float fTimeDiff = m_nIntervalTime / 1000000.0f;
            m_fFPS = (1.0f / fTimeDiff) * m_nDelayFrames;
            m_nIntervalTime = 0;
        }
    }

    void FPSCounter::recalculateStats(int nCycleUSec)
    {
        float fTimeMS = float(nCycleUSec) / 1000.0f;

        if (fTimeMS < m_fMinCycleTimeMS)
        {
            m_fMinCycleTimeMS = fTimeMS;
        }

        if (fTimeMS > m_fMaxCycleTimeMS)
        {
            m_fMaxCycleTimeMS = fTimeMS;
        }

        m_fLastCycleTimesMS[(m_nUpdates - 1) % 10] = fTimeMS;
    }

    // calculate difference between given time stamp and current time
    int FPSCounter::calculateTimeDiff(long& nSec, long& nUSec, bool bSetTime)
    {
        timeval t;
        gettimeofday(&t, nullptr);
        int nTimeDiff = (t.tv_sec - nSec) * 1000000 + ((int) t.tv_usec - nUSec);

        if (bSetTime)
        {
            nSec = t.tv_sec;
            nUSec = t.tv_usec;
        }
        return nTimeDiff;
    }
}
