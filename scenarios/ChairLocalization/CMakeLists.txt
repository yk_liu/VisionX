# Add your components below as shown in the following example:
#
# set(SCENARIO_COMPONENTS
#    ConditionHandler
#    Observer
#    PickAndPlaceComponent)

set(SCENARIO_COMPONENTS
	ObjectBoundingBoxLocalizerApp.cfg
    )

# optional 3rd parameter: "path/to/global/config.cfg"
#armarx_scenario("ChairLocalization" "${SCENARIO_COMPONENTS}")

set(SCENARIO_CONFIGS
	config/ObjectBoundingBoxLocalizerApp.Chair.cfg
    )
# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario_from_configs("ChairLocalization" "${SCENARIO_CONFIGS}")
