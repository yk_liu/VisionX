# Add your components below as shown in the following example:
#
# set(SCENARIO_COMPONENTS
#    ConditionHandler
#    Observer
#    PickAndPlaceComponenst)

set(SCENARIO_COMPONENTS
    TestImageProvider
    StreamProviderApp
    StreamImageProviderApp
)



# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario("TestImageProviderScenario" "${SCENARIO_COMPONENTS}")

#set(SCENARIO_CONFIGS
#    config/ComponentName.optionalString.cfg
#    )
# optional 3rd parameter: "path/to/global/config.cfg"
#armarx_scenario_from_configs("TestImageProviderScenario" "${SCENARIO_CONFIGS}")
