############################################################################################################################################################
FROM ubuntu:18.04 as base
############################################################################################################################################################
#install base packages
RUN apt-get update && apt-get upgrade -y
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata
RUN apt-get install -y apt-utils  gnupg2 curl ca-certificates openssh-client wget git gitk git-gui git-lfs g++-8 gcc-8 gfortran-8 cmake cmake-qt-gui            \
                       cmake-curses-gui mcpp libboost-all-dev libeigen3-dev libnlopt-dev freeglut3-dev qtbase5-dev  zeroc-ice-all-dev zeroc-ice-all-runtime     \
                       libdc1394-22-dev doxygen libgraphviz-dev libqwt-dev libpcre3-dev mongo-tools libmongoclient-dev mongodb-dev mongodb libjsoncpp-dev       \
                       libssl-dev libv4l-dev python-argcomplete python-pkg-resources python3-docutils python3-argcomplete python3-psutil libopencv-photo-dev    \
                       libopencv-ts-dev libopencv-superres-dev libopencv-stitching-dev libopencv-videostab-dev libx264-dev openni2-utils libsqlite3-dev         \
                       libgstreamer-plugins-base1.0-0 cppcheck lcov astyle libopencv-dev freeglut3-dev libnlopt-dev pcl-tools libpcl-dev libvtk6-dev libvtk6.3  \
                       qtbase5-dev qtwayland5 qttools5-dev libqwt-qt5-dev qtbase5-dev-tools qtscript5-dev qt5-default libqt5* libjemalloc-dev flex libbison-dev \
                       libfl-dev libssl-dev libbz2-dev libboost-all-dev libeigen3-dev iputils-ping iproute2 dnsutils traceroute net-tools nano
############################################################################################################################################################
#ht2 package server + package
RUN curl -fsSL https://packages.humanoids.kit.edu/h2t-key.pub | apt-key add - 
RUN echo "deb http://packages.humanoids.kit.edu/bionic/main bionic main" | tee /etc/apt/sources.list.d/armarx.list && \
    echo "deb http://packages.humanoids.kit.edu/bionic/testing bionic testing" >> /etc/apt/sources.list.d/armarx.list

RUN apt-get update
RUN apt-get install -y  h2t-libsoqt5-unspecified h2t-libsoqt5-dev h2t-libsoqt5-20 h2t-libsimage-dev h2t-libsimage h2t-libcoin80-dev h2t-libcoin80 h2t-libbullet
############################################################################################################################################################
#setup env / system
RUN ldconfig
RUN update-alternatives --install /usr/bin/gcc      gcc      /usr/bin/gcc-8      100
RUN update-alternatives --install /usr/bin/g++      g++      /usr/bin/g++-8      100
RUN update-alternatives --install /usr/bin/gfortran gfortran /usr/bin/gfortran-8 100

# avoid warnings during cmake in vision due to bug in vtk package: 
#https://stackoverflow.com/questions/37369369/compiling-pcl-1-7-on-ubuntu-16-04-errors-in-cmake-generated-makefile
RUN ln -s /usr/bin/vtk6 /usr/bin/vtk
RUN ln -s /usr/lib/python2.7/dist-packages/vtk/libvtkRenderingPythonTkWidgets.x86_64-linux-gnu.so /usr/lib/x86_64-linux-gnu/libvtkRenderingPythonTkWidgets.so
############################################################################################################################################################
#setup cuda
RUN curl -fsSL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub | apt-key add -                            && \
    echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /"             >  /etc/apt/sources.list.d/cuda.list      && \
    echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" >  /etc/apt/sources.list.d/nvidia-ml.list && \
    echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1704/x86_64 /"             >> /etc/apt/sources.list.d/cuda.list      && \
    echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1604/x86_64 /" >> /etc/apt/sources.list.d/nvidia-ml.list && \
    apt-get purge --autoremove -y curl                                                                                                            && \
    rm -rf /var/lib/apt/lists/*                                                                                                                   && \
    apt-get update

ENV CUDA_VERSION 9.0.176
ENV CUDA_PKG_VERSION 9-0=$CUDA_VERSION-1

### end 1st part from from https://gitlab.com/nvidia/cuda/blob/ubuntu18.04/10.0/base/Dockerfile
### 2nd part from https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.0/base/Dockerfile

RUN apt-get update && apt-get install -y --no-install-recommends \
        cuda-cudart-$CUDA_PKG_VERSION && \
    ln -s cuda-9.0 /usr/local/cuda && \
    rm -rf /var/lib/apt/lists/*

# CHANGED: commented out
# nvidia-docker 1.0
LABEL com.nvidia.volumes.needed="nvidia_driver"
LABEL com.nvidia.cuda.version="${CUDA_VERSION}"

RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
    echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf

ENV PATH /usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV NVIDIA_REQUIRE_CUDA "cuda>=9.0"

### end 2nd part from https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.0/base/Dockerfile

### all of https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.0/runtime/Dockerfile

ENV NCCL_VERSION 2.3.7

RUN apt-get update && apt-get install -y --no-install-recommends \
        cuda-libraries-$CUDA_PKG_VERSION \
        cuda-cublas-9-0=9.0.176.4-1 \
        libnccl2=$NCCL_VERSION-1+cuda9.0 && \
    apt-mark hold libnccl2 && \
    rm -rf /var/lib/apt/lists/*

### end all of from https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.0/runtime/Dockerfile

### all of https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.0/devel/Dockerfile

RUN apt-get update && apt-get install -y --no-install-recommends \
        cuda-libraries-dev-$CUDA_PKG_VERSION \
        cuda-nvml-dev-$CUDA_PKG_VERSION \
        cuda-minimal-build-$CUDA_PKG_VERSION \
        cuda-command-line-tools-$CUDA_PKG_VERSION \
        cuda-core-9-0=9.0.176.3-1 \
        cuda-cublas-dev-9-0=9.0.176.4-1 \
        libnccl-dev=$NCCL_VERSION-1+cuda9.0 && \
    rm -rf /var/lib/apt/lists/*

ENV LIBRARY_PATH /usr/local/cuda/lib64/stubs

### end all of https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.0/devel/Dockerfile

### all of https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.0/devel/cudnn7/Dockerfile

ENV CUDNN_VERSION 7.4.1.5
LABEL com.nvidia.cudnn.version="${CUDNN_VERSION}"

RUN apt-get update && apt-get install -y --no-install-recommends \
            libcudnn7=$CUDNN_VERSION-1+cuda9.0 \
            libcudnn7-dev=$CUDNN_VERSION-1+cuda9.0 && \
    apt-mark hold libcudnn7 && \
    rm -rf /var/lib/apt/lists/*

### end all of https://gitlab.com/nvidia/cuda/blob/ubuntu16.04/9.0/devel/cudnn7/Dockerfile

### start OpenGL

RUN apt-get update && apt-get install -y --no-install-recommends \
         pkg-config \
         libxau-dev \
         libxdmcp-dev \
         libxcb1-dev \
         libxext-dev \
         libx11-dev && \
     rm -rf /var/lib/apt/lists/*
COPY --from=nvidia/opengl:1.0-glvnd-runtime-ubuntu16.04 \
   /usr/local/lib/x86_64-linux-gnu \
   /usr/local/lib/x86_64-linux-gnu
COPY --from=nvidia/opengl:1.0-glvnd-runtime-ubuntu16.04 \
   /usr/local/share/glvnd/egl_vendor.d/10_nvidia.json \
   /usr/local/share/glvnd/egl_vendor.d/10_nvidia.json
RUN echo '/usr/lcal/lib/x86_64-linux-gnu' >> /etc/ld.so.conf.d/glvnd.conf && \
     ldconfig
RUN mkdir -p /usr/share/glvnd/egl_vendor.d && ln -s /usr/local/share/glvnd/egl_vendor.d/10_nvidia.json /usr/share/glvnd/egl_vendor.d/10_nvidia.json
ENV NVIDIA_VISIBLE_DEVICES \
     ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

### end of OpenGL

### most of https://github.com/tensorflow/tensorflow/blob/master/tensorflow/tools/dockerfiles/dockerfiles/gpu.Dockerfile

# Needed for string substitution 
SHELL ["/bin/bash", "-c"]
# Pick up some TF dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        libfreetype6-dev \
        libhdf5-serial-dev \
        libzmq3-dev \
        pkg-config \
        software-properties-common \
        unzip

# For MaskFusion
RUN apt-get install -y python3.6-dev virtualenv gcc-6 g++-6
RUN apt-get install -y libglew-dev
RUN apt-get install -y libsuitesparse-dev

# For CUDA profiling, TensorFlow requires CUPTI.
ENV LD_LIBRARY_PATH /usr/local/cuda/extras/CUPTI/lib64:$LD_LIBRARY_PATH

ARG USE_PYTHON_3_NOT_2
ARG _PY_SUFFIX=3
ARG PYTHON=python${_PY_SUFFIX}
ARG PIP=pip${_PY_SUFFIX}

# See http://bugs.python.org/issue19846
ENV LANG C.UTF-8

RUN apt-get update && apt-get install -y \
    ${PYTHON} \
    ${PYTHON}-pip

RUN ${PIP} --no-cache-dir install --upgrade \
    pip \
    setuptools

# Some TF tools expect a "python" binary
RUN ln -s $(which ${PYTHON}) /usr/local/bin/python 

# Options:
#   tensorflow
#   tensorflow-gpu
#   tf-nightly
#   tf-nightly-gpu
# Set --build-arg TF_PACKAGE_VERSION=1.11.0rc0 to install a specific version.
# Installs the latest version by default.
ARG TF_PACKAGE=tensorflow
ARG TF_PACKAGE_VERSION=1.10
RUN ${PIP} install tensorflow-gpu==1.10 backcall==0.1.0 django==2.2.2 image==1.5.27 ipython==7.5.0 ipython-genutils==0.2.0 jedi==0.13.3 joblib==0.13.2 mathutils==2.79.1  parso==0.4.0 pexpect==4.7.0 pickleshare==0.7.5 pillow==6.0.0 prompt-toolkit==2.0.9 ptyprocess==0.6.0 pygments==2.4.2 scikit-learn==0.21.2 six==1.12.0 sqlparse==0.3.0 traitlets==4.3.2 wcwidth==0.1.7

RUN ${PIP} install zeroc-ice==3.7.0
############################################################################################################################################################
#create user
# Replace 1000 with your user / group id
RUN mkdir -p /etc/sudoers.d && export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer
############################################################################################################################################################
#setup user
ENV HOME /home/developer
COPY copy/id_rsa $HOME/.ssh/id_rsa
RUN chown developer:developer $HOME/.ssh/id_rsa $HOME/.ssh/
USER developer
RUN chmod 700 $HOME/.ssh 
RUN chmod 600 $HOME/.ssh/id_rsa && echo -e "Host github.com \n    StrictHostKeyChecking no" > $HOME/.ssh/config
############################################################################################################################################################
#setup armarx
RUN cd && git clone https://gitlab.com/Simox/simox       && cd simox      && git checkout --track origin/Ubuntu18.04 && \
    cd && git clone https://gitlab.com/ivt/ivt                                                                       && \
    cd && git clone https://gitlab.com/ArmarX/ArmarXCore && cd ArmarXCore && git checkout --track origin/Ice3.7      && \
    cd && git clone https://gitlab.com/ArmarX/ArmarXGui  && cd ArmarXGui  && git checkout --track origin/Ice3.7      && \
    cd && git clone https://gitlab.com/ArmarX/RobotAPI   && cd RobotAPI   && git checkout --track origin/Ice3.7      && \
    cd && git clone https://gitlab.com/ArmarX/MemoryX    && cd MemoryX    && git checkout --track origin/Ice3.7      && \
    cd && git clone https://gitlab.com/ArmarX/VisionX    && cd VisionX    && git checkout --track origin/Ice3.7      && \
    cd && git clone https://gitlab.com/ArmarX/python3-armarx

RUN cd $HOME/VisionX/data/VisionX/maskfusion/ && git pull && bash install_maskfusion.sh

############################################################################################################################################################
RUN cd $HOME/ArmarXCore/build && cmake ..
RUN cd $HOME/simox/build      && cmake ..
RUN cd $HOME/ArmarXGui/build  && cmake ..
RUN cd $HOME/RobotAPI/build   && cmake ..; exit 0
RUN cd $HOME/ivt/build        && cmake ..
RUN cd $HOME/MemoryX/build    && cmake ..; exit 0
RUN cd $HOME/VisionX/build    && cmake ..; exit 0
############################################################################################################################################################
#setup tracking


RUN $HOME/ArmarXCore/build/bin/armarx -y profile
#RUN $HOME/ArmarXCore/build/bin/armarx -y changeHost --host 10.6.2.101 --port 4061
RUN echo ". $HOME/.bashrc" >> $HOME/.profile
RUN echo -e "[AutoCompletion]\narmarx=0\npackages=ArmarXCore,ArmarXGui" >> $HOME/.armarx/armarx.ini
RUN cd $HOME/python3-armarx && pip install --user -e  .
#RUN . ~/.bashrc && pip install lxml

WORKDIR $HOME
RUN echo "UPDATE"

RUN cd $HOME/simox/build      && make -j16
RUN $HOME/ArmarXCore/build/bin/armarx-dev -y build RobotAPI
RUN cd $HOME/ivt/build        && make -j16
RUN $HOME/ArmarXCore/build/bin/armarx-dev -y build MemoryX
RUN $HOME/ArmarXCore/build/bin/armarx-dev -y build VisionX


USER root
RUN apt-get install -y gedit
RUN apt-get install -y mesa-utils
USER developer

# Copy the python files to VisionX bin folder
RUN cp ~/VisionX/data/VisionX/maskfusion/maskfusion/build/GUI/MaskRCNN.py ~/VisionX/build/bin/
RUN cp ~/VisionX/data/VisionX/maskfusion/maskfusion/build/GUI/helpers.py ~/VisionX/build/bin/
RUN cp ~/VisionX/data/VisionX/maskfusion/maskfusion/build/GUI/config.toml ~/VisionX/build/bin/

# Make sure to preload python 3.6 because ArmarX links also python 2.7 (uggh)
ENV LD_PRELOAD /usr/lib/x86_64-linux-gnu/libpython3.6m.so.1.0

RUN $HOME/ArmarXCore/build/bin/armarx -y changeHost --host 10.6.2.101 --port 4061

RUN git clone https://sh-ocado1:ARMAR-6-SH!@i61wiki.itec.uka.de/git/armar6rt.git Armar6RT && cd Armar6RT && git checkout Armar6TestrigIce3.7
RUN cd Armar6RT/build && cmake ..

RUN cd ~/VisionX/build && git pull && make -j5
RUN echo -e "export PATH=$HOME/ArmarXCore/build/bin/:$PATH" >> ~/.bashrc 


#RUN cd ArmarXGui && git   pull
#RUN ~/ArmarXCore/build/bin/armarx-dev build VisionX
ENV CUDA_VISIBLE_DEVICES 0,1,3
#-DMASKFUSION_GPU_SLAM=1
RUN cd ~/VisionX/data/VisionX/maskfusion/maskfusion/build && cmake ..  && make -j50
RUN ~/ArmarXCore/build/bin/armarx-dev build VisionX --no-deps
RUN echo -e "#!/bin/bash\n~/VisionX/build/bin/CoFusionProcessorAppRun  --ArmarX.CoFusionProcessor.ImageProviderName=OpenNIPointCloudProvider --ArmarX.CoFusionProcessor.cofusion.DeviceMaskRCNN=3" >> ~/run-maskfusion.sh
RUN chmod +x ~/run-maskfusion.sh
ENV PANGOLIN_WINDOW_URI=headless://
#RUN $HOME/ArmarXCore/build/bin/armarx -y changeHost --host 10.6.0.253 --port 4061


############################################################################################################################################################


