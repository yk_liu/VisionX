#!/bin/bash
xhost +local:root
# Need enough shared memory (shm-size) for PointCloud shared memory provider in ArmarX (otherwise SIGBUS)
# Need Display and X11 support, because MaskFusion opens a window using Pangolin
# Use network of the thinkstation
# SYS_PTRACE: Enable debugging

nvidia-docker run --cap-add=SYS_PTRACE --security-opt seccomp=unconfined --shm-size 1G --network thinkstation  -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --rm -it maskfusion  bash -il
