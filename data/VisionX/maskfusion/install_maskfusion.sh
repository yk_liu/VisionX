#!/bin/bash

function make_path_absolute() {
  if which realpath >/dev/null; then
    tmp_pwd=`pwd`
    cd $OLD_PWD
    realpath $1
    cd $tmp_pwd
  else
    echo $1
  fi
}
OLD_PWD=`pwd`
if [ -z "$DIR" ]; then
  DIR=`pwd`
else
  DIR=`make_path_absolute $DIR`
fi
echo "Using $DIR as working directory"


#if [ -z "${CUSTOM_CUDA+x}" ] && [ ! -d "$DIR/CUDA9" ]; then
#  echo "Installing CUDA 9.0 to ${DIR}/CUDA9"
#  cd $DIR
#  if [ ! -f CUDA9.deb ]; then
#    wget -O CUDA9.deb 'https://developer.nvidia.com/compute/cuda/9.0/Prod/local_installers/cuda-repo-#ubuntu1704-9-0-local_9.0.176-1_amd64-deb'
#  fi
#  mkdir "$DIR/CUDA9"
#  dpkg-deb -xv "$DIR/CUDA9.deb" "$DIR/CUDA9"
#  find "$DIR/CUDA9" -type f -name '*.deb' -exec dpkg-deb -xv {} "$DIR/CUDA9" \;
#fi
#if [ ! -d "$DIR/CUDA9" ]; then
#  export CUDA_BIN_PATH="$DIR/CUDA9/usr/local/cuda-9.0"
#fi

echo "Installing MaskFusion to $DIR/maskfusion"
cd $DIR
if [ ! -d "$DIR/maskfusion" ]; then
  echo "Cloning maskfusion into $DIR/maskfusion"
  git clone -- "https://github.com/martinruenz/maskfusion.git" "$DIR/maskfusion"
fi
cd "$DIR/maskfusion"

##################

function highlight(){
  echo "$1"
}

# Function that executes the clone command given as $1 iff repo does not exist yet. Otherwise pulls.
# Only works if repository path ends with '.git'
# Example: git_clone "git clone --branch 3.4.1 --depth=1 https://github.com/opencv/opencv.git"
function git_clone(){
  repo_dir=`basename "$1" .git`
  git -C "$repo_dir" pull 2> /dev/null || eval "$1"
}

#source ~/miniconda/etc/profile.d/conda.sh
#conda create --yes -n maskfusion python=3.6
#conda activate maskfusion

#highlight "Installinc CUDA"
#conda install --yes pip
#conda install --yes -c anaconda cudatoolkit=9.0

virtualenv -p python3.6 python-environment
source python-environment/bin/activate

highlight "Installing pip packages"
pip install -U pip
pip install tensorflow-gpu==1.10\
    scikit-image\
    keras\
    IPython\
    h5py\
    cython\
    imgaug==0.2.6\
    numpy==1.14.5\
    opencv-python\
    pytoml\
    matplotlib==2.1.1

# Added by me
pip install pycocotools

# Provide numpy headers to C++
ln -s `python -c "import numpy as np; print(np.__path__[0])"`/core/include/numpy Core/Segmentation/MaskRCNN || true

# fix version check of h5py
sed -i 's/plist.set_libver_bounds(low, high)/plist.set_libver_bounds(low, 1)/g' $DIR/maskfusion/python-environment/lib/python3.6/site-packages/h5py/_hl/files.py

# Build dependencies
highlight "Building dependencies"
mkdir -p deps
cd deps

# Pangolin
highlight "Building pangolin..."
git_clone "git clone --depth=1 https://github.com/stevenlovegrove/Pangolin.git"
cd Pangolin
mkdir -p build
cd build
cmake -DAVFORMAT_INCLUDE_DIR="" ..
make -j8
export Pangolin_DIR=$(pwd)
echo "Pangolin_DIR: $Pangolin_DIR"
cd ../..

# build freetype-gl-cpp
highlight "Building freetype-gl-cpp..."
git_clone "git clone --depth=1 --recurse-submodules https://github.com/martinruenz/freetype-gl-cpp.git"
cd freetype-gl-cpp
mkdir -p build
cd build
cmake -DBUILD_EXAMPLES=OFF -DCMAKE_INSTALL_PREFIX="`pwd`/../install" -DCMAKE_BUILD_TYPE=Release ..
make -j8
make install
cd ../..


# build DenseCRF, see: http://graphics.stanford.edu/projects/drf/
highlight "Building densecrf..."
git_clone "git clone --depth=1 https://github.com/martinruenz/densecrf.git"
cd densecrf
mkdir -p build
cd build
cmake \
-DCMAKE_BUILD_TYPE=Release \
-DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -fPIC" \
..
make -j8
cd ../..

# build gSLICr, see: http://www.robots.ox.ac.uk/~victor/gslicr/
highlight "Building gslicr..."
git_clone "git clone --depth=1 https://github.com/carlren/gSLICr.git"
cd gSLICr
mkdir -p build
cd build
# WARNING: Have to compile with gcc-6 to work with CUDA 9.0
cmake \
-DOpenCV_DIR="${OpenCV_DIR}" \
-DCMAKE_BUILD_TYPE=Release \
-DCUDA_HOST_COMPILER=/usr/bin/gcc-6 \
-DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -D_FORCE_INLINES" \
..
make -j8
cd ../..


# Prepare MaskRCNN and data
highlight "Building mask-rcnn with ms-coco..."
git_clone "git clone --depth=1 https://github.com/matterport/Mask_RCNN.git"
git_clone "git clone --depth=1 https://github.com/waleedka/coco.git"
cd coco/PythonAPI
make
make install # Make sure to source the correct python environment first
cd ../..
cd Mask_RCNN
mkdir -p data
cd data
wget --no-clobber https://github.com/matterport/Mask_RCNN/releases/download/v1.0/mask_rcnn_coco.h5
cd ../..

# c++ toml
highlight "Building toml11..."
git_clone "git clone --depth=1 https://github.com/ToruNiina/toml11.git"


# End of dependencies
cd ..

# Build MaskFusion
highlight "Building MaskFusion..."
mkdir -p build
cd build
ln -s ../deps/Mask_RCNN ./ || true # Also, make sure that the file 'mask_rcnn_model.h5' is linked or present
cmake \
  -DBOOST_ROOT="${BOOST_ROOT}" \
  -DOpenCV_DIR="$(pwd)/../deps/opencv/build" \
  -DPangolin_DIR="$(pwd)/../deps/Pangolin/build/src" \
  -DMASKFUSION_PYTHON_VE_PATH="$(pwd)/../python-environment" \
  -DCUDA_HOST_COMPILER=/usr/bin/gcc-6 \
  -DWITH_FREENECT2=OFF \
  ..
make -j8
cd ..

##################


echo "Run the following commands to be able to execute the CoFusionProcessor:"
if [ -z ${CUSTOM_CUDA+x} ]; then
  echo "echo \"export LD_LIBRARY_PATH=\\\"$DIR/CUDA9/usr/local/cuda-9.0/targets/x86_64-linux/lib/\\\"\" >> $HOME/.bashrc"
fi
echo "cp $DIR/maskfusion/build/GUI/config.toml \${VisionX_DIR}/build/bin"
echo "cp $DIR/maskfusion/build/GUI/*.py \${VisionX_DIR}/build/bin"
echo
echo "To execute CoFusionProcessor make shure Python3 is loaded first:"
echo "export LD_PRELOAD=/path/to/libpython3"

cd $OLD_PWD
