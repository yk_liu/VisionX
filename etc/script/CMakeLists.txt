add_subdirectory(multisense) 

# Add a combined target for all script symlinks
add_custom_target(VisionXScripts ALL DEPENDS 
    VisionXScriptMultiSenseStart 
    VisionXScriptMultiSenseStop 
    )
