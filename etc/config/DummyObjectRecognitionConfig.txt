VisionX.DummyObjectRecognition.Object.Name = Amicelli
VisionX.DummyObjectRecognition.Object.Pose.x = 1.0
VisionX.DummyObjectRecognition.Object.Pose.y = 2.0
VisionX.DummyObjectRecognition.Object.Pose.z = 3.0
VisionX.DummyObjectRecognition.Object.Pose.qw = 4.0
VisionX.DummyObjectRecognition.Object.Pose.qx = 5.0
VisionX.DummyObjectRecognition.Object.Pose.qy = 6.0
VisionX.DummyObjectRecognition.Object.Pose.qz = 7.0
VisionX.DummyObjectRecognition.Object.RecognitionCertainty = 0.9
VisionX.DummyObjectRecognition.LocalizationDelay = 100

