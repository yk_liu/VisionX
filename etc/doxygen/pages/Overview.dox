/*!
 * \page VisionX-Overview VisionX Overview
 *
 * \section VisionX-Overview-purpose Purpose
 *
 * VisionX provides mechanisms to handle images and pointclouds in ArmarX.
 * This includes image and pointcloud transport, either locally or over network.
 * For local transfer, shared memory is used. For network transfer, Ice
 * is used.
 *
 * The two main component interfaces are:
 * - visionx::ImageProvider: provides images to other components
 * - visionx::ImageProcessor: retrieves images from providers and process them
 *
 *   \image html VisionXStructure.bmp "The two main components provided by VisionX are image providers and image processors"
 *
 *
 * The same concept is followed for pointclouds with the visionx::PointCloud::PointCloudProvider and visionx::PointCloud::PointCloudProcessor.
 *
 * When both images and pointclouds are provided, the visionx::PointCloud::CapturingPointCloudAndImageProviderInterface should be implemented. Components that
 * process both images and pointclouds can inherit from the visionx::PointCloud::PointCloudAndImageProcessor.
 *
 * Vision algorithms that can be used by different components, and therefore should be compiled to be a small lib, are located in the folder vision_algorithms.
 *
 *
 * \section VisionX-Overview-objectlocalization Object localization
 *
 * When objects need to be localized, this is triggered and the location information is obtained via MemoryX. Information about that can be
 * found here: \ref memoryx-howto-retrieve-objects "How to retrieve an object from MemoryX"
 *
 * An explanation how to implement your own object localizer can be found here: \ref VisionX-HowTos-objectlocalization "How to implement an object localizer"
 
\section VisionX-Overview-pointcloud Point cloud components

Point clouds, a larger set of 3D points, allow for creating a discretized
representation of the current scene.  In general, RGB-D images  are captured
with an active camera or stereo vision system and then processed to a point
cloud data format.

\image html rgbd_image.png "RGB and depth image captured with an Asus Xtion PRO camera"

Please refer to the howto \ref VisionX-HowTos-implement-pointcloudprovider
"Implementing a PointCloudProvider" if you wish to implement your on point
cloud provider.  A detailed description how to implement your own point cloud
processer can be found here:
 \ref VisionX-HowTos-HowTos-implement-pointcloudprocessor "Implementing a generic PointCloudProcessor"

 * \subsection VisionX-Overview-interface Interface Documentation
 * Documentation of interfaces available in ArmarXCore is available on the \ref slicedocumentation "Slice Documentation" page.
 */
